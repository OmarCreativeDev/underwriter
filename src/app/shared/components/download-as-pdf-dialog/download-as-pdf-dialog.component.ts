import * as autoTable from 'jspdf-autotable';
import * as jsPDF from 'jspdf';
import { accountColumns, accountLabels, IAccount } from '../../../services/account/account.interface';
import { AccountService } from '../../../services/account/account.service';
import { ApplicationService } from '../../../services/application/application.service';
import { IFinanceMonth, IFinanceRecord } from '../../../services/finance/finance.interface';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FinanceService } from '../../../services/finance/finance.service';
import { first } from 'rxjs/operators';
import { forkJoin, Observable } from 'rxjs';
import { HttpErrorResponse } from '@angular/common/http';
import { IApplication } from '../../../services/application/application.interface';
import { ITransaction, allAccountsTransactionsMap } from '../../../services/transaction/transaction.interface';
import { MatDialogRef } from '@angular/material';
import { OrderByPipe } from '../../pipes/order-by/order-by.pipe';
import { TransactionService } from '../../../services/transaction/transaction.service';

jsPDF.autoTable = autoTable;

@Component({
  selector: 'app-download-as-pdf-dialog',
  templateUrl: './download-as-pdf-dialog.component.html',
  styleUrls: ['./download-as-pdf-dialog.component.scss'],
})
export class DownloadAsPdfDialogComponent implements OnInit {

  public pdf: jsPDF = new jsPDF('landscape', 'pt');
  public loading: boolean = true;
  public error: string;

  constructor(
    private accountService: AccountService,
    private applicationService: ApplicationService,
    private financeService: FinanceService,
    private transactionService: TransactionService,
    public dialogRef: MatDialogRef<DownloadAsPdfDialogComponent>,
    private orderByPipe: OrderByPipe,
    private datePipe: DatePipe,
  ) {}

  ngOnInit(): void {
    this.getPartyId();
  }

  public getPartyId(): void {
    this.applicationService.partyIdStream
      .pipe(first())
      .subscribe((partyId: number) => {
        this.setupListener(partyId);
      });
  }

  public setupListener(partyId: number): void {
    const dataStreams: Array<Observable<any>> = [
      this.applicationService.get(partyId),
      this.accountService.list(partyId),
      this.financeService.listAffordabilitySummaries(partyId, true),
      this.financeService.listAffordabilitySummaries(partyId, false),
      this.transactionService.listAllTransactionsByFilter(partyId),
    ];

    forkJoin(...dataStreams)
      .pipe(first())
      .subscribe((
        response: [
          IApplication, Array<IAccount>, Array<IFinanceRecord>, Array<IFinanceRecord>, Array<ITransaction>
        ]) => {
          this.loading = false;
          this.generatePdf(response[0], response[1], response[2], response[3], response[4]);
        },
        (httpErrorResponse: HttpErrorResponse) => {
          this.loading = false;
          this.error = `${httpErrorResponse.statusText}, code: ${httpErrorResponse.status}`;
        });
  }

  public getAccountsListColumns(): Array<Object> {
    return [
      { title: accountLabels[0], dataKey: 'providerName' },
      { title: accountLabels[1], dataKey: accountColumns[1] },
      { title: accountLabels[2], dataKey: accountColumns[2] },
      { title: accountLabels[3], dataKey: accountColumns[3] },
      { title: accountLabels[4], dataKey: accountColumns[4] },
    ];
  }

  public getSummariesListColumns(summariesList: IFinanceRecord): Array<Object> {
    const shortMonthNames: Array<string> = this.financeService.generateMonths(summariesList.data);
    const fullMonthNames: Array<string> = this.financeService.generateMonths(summariesList.data, true);
    const columns: Array<Object> = [
      { title: '', dataKey: 'metric' }
    ];

    for (let i = 0; i < shortMonthNames.length; i++) {
      columns.push({
        title: shortMonthNames[i],
        dataKey: fullMonthNames[i],
      });
    }

    columns.push({ title: 'Average', dataKey: 'averageAmount' });

    return columns;
  }

  /**
   * Prepend currency symbol to monthly values
   * And sort list
   * @param summariesList
   */
  public modifySummariesList(summariesList: Array<IFinanceRecord>): Array<IFinanceRecord> {
    summariesList.forEach((summary: IFinanceRecord) => {
      summary.data.forEach((item: IFinanceMonth) => item.totalAmount = `£${item.totalAmount}`);
    });

    return this.orderByPipe.transform(summariesList, 'code', 'asc');
  }

  public orderAllAccountsTransactions(allTransactionsList: Array<ITransaction>): Array<Object> {
    return this.orderByPipe.transform(allTransactionsList, ['accountNumber', 'date'], ['desc', 'desc']);
  }

  public addApplicationDetailsToPdf(applicationDetails: IApplication): void {
    this.pdf.setFontStyle('bold');
    this.pdf.setFontSize(18);
    this.pdf.text('Application Details', 10, 20);

    this.pdf.setFontSize(12);
    this.pdf.setFontStyle('normal');
    this.pdf.text(`Name: ${applicationDetails.name}`, 10, 40);
    this.pdf.text(`Date submitted: ${this.datePipe.transform(applicationDetails.updatedOn, 'dd/MM/yyyy')}`, 10, 60);
    this.pdf.text(`Post code: ${applicationDetails.postcode}`, 10, 80);
    this.pdf.text(`Date of birth: ${this.datePipe.transform(applicationDetails.dateOfBirth, 'dd/MM/yyyy')}`, 10, 100);
    this.pdf.text(`Email address: ${applicationDetails.email}`, 10, 120);
    this.pdf.text(`Reference No: ${applicationDetails.referenceNumber}`, 10, 140);

    this.pdf.setFontSize(18);
    this.pdf.setFontStyle('bold');
  }

  public addSectionToPdf(
    sectionName: string,
    sectionColumns: Array<Object>,
    sectionRows: Array<Object>,
    columnStyles?: Object,
  ): void {
    const topPosition: number = this.pdf.autoTable.previous.finalY === undefined ? 150 : this.pdf.autoTable.previous.finalY;

    this.pdf.text(sectionName, 10, topPosition + 40);
    this.pdf.autoTable(
      sectionColumns,
      sectionRows,
      {
        startY: topPosition + 50,
        margin: { horizontal: 10 },
        styles: { overflow: 'linebreak' },
        columnStyles: columnStyles || {},
      }
    );
  }

  public generatePdf(
    applicationDetails: IApplication,
    accountsList: Array<IAccount>,
    incomeSummariesList: Array<IFinanceRecord>,
    expenseSummariesList: Array<IFinanceRecord>,
    allAccountsTransactions: Array<ITransaction>,
  ): void {
    this.addApplicationDetailsToPdf(applicationDetails);

    this.addSectionToPdf('Accounts list', this.getAccountsListColumns(), accountsList);

    this.addSectionToPdf(
      'Income',
      this.getSummariesListColumns(incomeSummariesList[0]),
      this.modifySummariesList(incomeSummariesList),
    );

    this.addSectionToPdf(
      'Expense',
      this.getSummariesListColumns(expenseSummariesList[0]),
      this.modifySummariesList(expenseSummariesList),
    );

    this.addSectionToPdf(
      'Transactions',
      allAccountsTransactionsMap,
      this.orderAllAccountsTransactions(allAccountsTransactions),
      {
        categorisation: { columnWidth: 100 },
        fcaCategory: { columnWidth: 90 },
        categorisationCode: { columnWidth: 70 },
        balance: { columnWidth: 60 },
        amount: { columnWidth: 60 },
      }
    );

    this.pdf.save(`${applicationDetails.referenceNumber}.pdf`);
    this.dialogRef.close();
  }

}
