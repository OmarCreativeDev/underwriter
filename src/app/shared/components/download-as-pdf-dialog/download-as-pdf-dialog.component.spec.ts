import { accountMock, AccountServiceMock } from '../../../services/account/account.service.mock';
import { AccountService } from '../../../services/account/account.service';
import { applicationMock, ApplicationServiceMock } from '../../../services/application/application.service.mock';
import { ApplicationService } from '../../../services/application/application.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DatePipe } from '@angular/common';
import { DatePipeMock } from '../../pipes/date.pipe.mock';
import { DownloadAsPdfDialogComponent } from './download-as-pdf-dialog.component';
import { financeMock, FinanceServiceMock } from '../../../services/finance/finance.service.mock';
import { FinanceService } from '../../../services/finance/finance.service';
import { MatDialogMock } from '../../modules/material/mat-dialog.mock';
import { MatDialogRef } from '@angular/material';
import { OrderByPipe } from '../../pipes/order-by/order-by.pipe';
import { OrderByPipeMock } from '../../pipes/order-by/order-by.pipe.mock';
import { transactionMock, TransactionServiceMock } from '../../../services/transaction/transaction.service.mock';
import { TransactionService } from '../../../services/transaction/transaction.service';

xdescribe('DownloadAsPdfDialogComponent', () => {
  let component: DownloadAsPdfDialogComponent;
  let fixture: ComponentFixture<DownloadAsPdfDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        DownloadAsPdfDialogComponent,
      ],
      providers: [
        { provide: AccountService, useClass: AccountServiceMock },
        { provide: ApplicationService, useClass: ApplicationServiceMock },
        { provide: FinanceService, useClass: FinanceServiceMock },
        { provide: TransactionService, useClass: TransactionServiceMock },
        { provide: MatDialogRef, useClass: MatDialogMock },
        { provide: OrderByPipe, useClass: OrderByPipeMock },
        { provide: DatePipe, useClass: DatePipeMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadAsPdfDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` should invoke `getPartyId()`', () => {
    spyOn(component, 'getPartyId');
    component.ngOnInit();
    expect(component.getPartyId).toHaveBeenCalled();
  });

  it('`getPartyId()` should invoke `setupListener()`', () => {
    spyOn(component, 'setupListener');
    component.getPartyId();
    expect(component.setupListener).toHaveBeenCalled();
  });

  it('`setupListener()` should invoke `generatePdf()`', () => {
    spyOn(component, 'generatePdf');
    component.setupListener(99999);
    expect(component.generatePdf).toHaveBeenCalled();
  });

  it('`getAccountsListColumns()` should return an Array of Objects`', () => {
    const accountsListColumns: Array<Object> = component.getAccountsListColumns();
    expect(accountsListColumns).toBeDefined();
    expect(accountsListColumns.length).toEqual(5);
  });

  it('`getSummariesListColumns()` should return an Array of Objects`', () => {
    const summariesListColumns: Array<Object> = component.getSummariesListColumns(financeMock);
    expect(summariesListColumns).toBeDefined();
    expect(summariesListColumns.length).toEqual(5);
  });

  it('`addApplicationDetailsToPdf()` should add details of application to pdf`', () => {
    spyOn(component.pdf, 'setFontStyle');
    spyOn(component.pdf, 'setFontSize');
    spyOn(component.pdf, 'text');

    component.addApplicationDetailsToPdf(applicationMock);
    expect(component.pdf.setFontStyle).toHaveBeenCalledTimes(3);
    expect(component.pdf.setFontSize).toHaveBeenCalledTimes(3);
    expect(component.pdf.text).toHaveBeenCalledTimes(7);
  });

  it('`addSectionToPdf()` should add section to pdf`', () => {
    spyOn(component.pdf, 'text');
    spyOn(component.pdf, 'autoTable');
    spyOn(component, 'getAccountsListColumns').and.returnValue([
      { title: 'some title', dataKey: 'providerName' }
    ]);

    component.addSectionToPdf(
      'someCoolSection',
      component.getAccountsListColumns(),
      [applicationMock],
    );
    expect(component.pdf.text).toHaveBeenCalled();
    expect(component.pdf.autoTable).toHaveBeenCalled();
  });

  it('`generatePdf()` should add sections to pdf, generate PDF and close dialog`', () => {
    spyOn(component, 'addApplicationDetailsToPdf').and.returnValue(false);
    spyOn(component, 'addSectionToPdf');
    spyOn(component.pdf, 'save').and.returnValue(false);
    spyOn(component.dialogRef, 'close').and.returnValue(false);

    component.generatePdf(
      applicationMock,
      [accountMock],
      [financeMock],
      [financeMock],
      [transactionMock],
    );

    expect(component.addApplicationDetailsToPdf).toHaveBeenCalled();
    expect(component.addSectionToPdf).toHaveBeenCalledTimes(4);
    expect(component.pdf.save).toHaveBeenCalled();
    expect(component.dialogRef.close).toHaveBeenCalled();
  });
});
