import { Component, OnDestroy, OnInit } from '@angular/core';
import { IPartnerLogo } from '../../../services/partner/partner.interface';
import { PartnerService } from '../../../services/partner/partner.service';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  private componentAlive: boolean = true;
  public partnerLogo: string;

  constructor(
    private partnerService: PartnerService,
  ) {}

  ngOnInit(): void {
    this.fetchLogo();
  }

  private fetchLogo(): void {
    this.partnerService.fetchLogo()
      .pipe(takeWhile(() => this.componentAlive))
      .subscribe((partnerLogo: IPartnerLogo) => {
        this.partnerLogo = partnerLogo.imageUrl;
      });
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
