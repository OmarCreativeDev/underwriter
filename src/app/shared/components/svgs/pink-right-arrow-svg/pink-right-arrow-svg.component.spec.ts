import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PinkRightArrowSvgComponent } from './pink-right-arrow-svg.component';

describe('PinkRightArrowSvgComponent', () => {
  let component: PinkRightArrowSvgComponent;
  let fixture: ComponentFixture<PinkRightArrowSvgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PinkRightArrowSvgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PinkRightArrowSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
