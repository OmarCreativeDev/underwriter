import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperianLogoSvgComponent } from './experian-logo-svg.component';

describe('ExperianLogoSvgComponent', () => {
  let component: ExperianLogoSvgComponent;
  let fixture: ComponentFixture<ExperianLogoSvgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExperianLogoSvgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperianLogoSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
