import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpArrowSvgComponent } from './up-arrow-svg.component';

describe('UpArrowSvgComponent', () => {
  let component: UpArrowSvgComponent;
  let fixture: ComponentFixture<UpArrowSvgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpArrowSvgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpArrowSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
