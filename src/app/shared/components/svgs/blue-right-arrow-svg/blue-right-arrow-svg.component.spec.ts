import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BlueRightArrowSvgComponent } from './blue-right-arrow-svg.component';

describe('BlueRightArrowSvgComponent', () => {
  let component: BlueRightArrowSvgComponent;
  let fixture: ComponentFixture<BlueRightArrowSvgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlueRightArrowSvgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlueRightArrowSvgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
