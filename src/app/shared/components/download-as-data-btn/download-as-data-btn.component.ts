import { ApplicationService } from '../../../services/application/application.service';
import { Component, OnInit } from '@angular/core';
import { DownloadAsPdfDialogComponent } from '../download-as-pdf-dialog/download-as-pdf-dialog.component';
import { first } from 'rxjs/operators';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-download-as-data-btn',
  templateUrl: './download-as-data-btn.component.html',
})
export class DownloadAsDataBtnComponent implements OnInit {

  public downloadTypes: Array<string> = ['CSV', 'PDF'];
  public partyId: number;
  public selectedDownloadType: string = this.downloadTypes[0];

  constructor(
    private applicationService: ApplicationService,
    public matDialog: MatDialog,
  ) {}

  ngOnInit(): void {
    this.getPartyId();
  }

  public getPartyId(): void {
    this.applicationService.partyIdStream
      .pipe(first())
      .subscribe((partyId: number) => {
        this.partyId = partyId;
      });
  }

  /**
   * This method is invoked by the radio group
   * Once a radio button selection is made
   * @param type
   */
  public setDownloadType(type: string): void {
    this.selectedDownloadType = type;
  }

  /**
   * This method checks the selected download type
   * And then invokes the correct corresponding method
   */
  public handleDownloadType(): void {
    if (this.selectedDownloadType === this.downloadTypes[0]) {
      this.downloadAsCsv();
    } else {
      this.openDownloadAsPdfDialog();
    }
  }

  /**
   * This method opens is responsible for
   * opening up a new web page pointing to a zip file
   * containing csv files
   */
  public downloadAsCsv(): void {
    window.open(`csv/zip?partyId=${this.partyId}`, '_blank');
  }

  /**
   * This method opens a dialog component
   * Which initiliases specific components
   * In order to be displayed and exported as a pdf
   */
  public openDownloadAsPdfDialog(): void {
    this.matDialog.open(DownloadAsPdfDialogComponent, { disableClose: true });
  }

}
