import { ApplicationService } from '../../../services/application/application.service';
import { ApplicationServiceMock } from '../../../services/application/application.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DownloadAsDataBtnComponent } from './download-as-data-btn.component';
import { MatDialog } from '@angular/material';
import { MatDialogMock } from '../../modules/material/mat-dialog.mock';

describe('DownloadAsDataBtnComponent', () => {
  let component: DownloadAsDataBtnComponent;
  let fixture: ComponentFixture<DownloadAsDataBtnComponent>;
  let applicationService: ApplicationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DownloadAsDataBtnComponent ],
      providers: [
        { provide: ApplicationService, useClass: ApplicationServiceMock },
        { provide: MatDialog, useClass: MatDialogMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DownloadAsDataBtnComponent);
    component = fixture.componentInstance;
    applicationService = TestBed.get(ApplicationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` should invoke `getPartyId()`', () => {
    spyOn(component, 'getPartyId');
    component.ngOnInit();
    expect(component.getPartyId).toHaveBeenCalled();
  });

  it('`getPartyId()` should set `partyId`', () => {
    applicationService.partyIdStream.next(999999);
    component.getPartyId();

    expect(component.partyId).toBeDefined();
    expect(component.partyId).toEqual(999999);
  });

  it('`setDownloadType()` should set `selectedDownloadType`', () => {
    component.setDownloadType(component.downloadTypes[0]);
    expect(component.selectedDownloadType).toBeDefined();
    expect(component.selectedDownloadType).toEqual(component.downloadTypes[0]);
  });

  it('`handleDownloadType()` should invoke `downloadAsCsv()` if download type is `CSV`', () => {
    spyOn(component, 'downloadAsCsv');

    component.selectedDownloadType = component.downloadTypes[0];
    component.handleDownloadType();

    expect(component.downloadAsCsv).toHaveBeenCalled();
  });

  it('`handleDownloadType()` should invoke `openDownloadAsPdfDialog()` if download type is `PDF`', () => {
    spyOn(component, 'openDownloadAsPdfDialog');

    component.selectedDownloadType = component.downloadTypes[1];
    component.handleDownloadType();

    expect(component.openDownloadAsPdfDialog).toHaveBeenCalled();
  });

  it('`downloadAsCsv()` should invoke `window.open()`', () => {
    spyOn(window, 'open');
    component.downloadAsCsv();
    expect(window.open).toHaveBeenCalled();
  });

  it('`openDownloadAsPdfDialog()` should invoke `matDialog.open()`', () => {
    spyOn(component.matDialog, 'open');
    component.openDownloadAsPdfDialog();
    expect(component.matDialog.open).toHaveBeenCalled();
  });
});
