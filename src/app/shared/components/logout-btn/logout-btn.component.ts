import { Component } from '@angular/core';
import { AuthService } from '../../../services/auth/auth.service';

@Component({
  selector: 'app-logout-btn',
  templateUrl: './logout-btn.component.html',
})
export class LogoutBtnComponent {

  constructor(
    private authService: AuthService,
  ) {}

  public logout(): void {
    this.authService.logout();
  }

}
