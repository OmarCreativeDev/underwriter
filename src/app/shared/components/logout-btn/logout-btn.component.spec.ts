import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AuthService } from '../../../services/auth/auth.service';
import { AuthServiceMock } from '../../../services/auth/auth.service.mock';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { LogoutBtnComponent } from './logout-btn.component';

describe('LogoutBtnComponent', () => {
  let component: LogoutBtnComponent;
  let fixture: ComponentFixture<LogoutBtnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogoutBtnComponent ],
      providers: [
        { provide: AuthService, useClass: AuthServiceMock }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
