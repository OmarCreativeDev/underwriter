import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-breadcrumb-link',
  templateUrl: './breadcrumb-link.component.html',
  styleUrls: ['./breadcrumb-link.component.scss']
})
export class BreadcrumbLinkComponent {

  @Input() public label: string;
  @Input() public location: string;

}
