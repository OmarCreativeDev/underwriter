import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BreadcrumbLinkComponent } from './breadcrumb-link.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';

describe('BreadcrumbLinkComponent', () => {
  let component: BreadcrumbLinkComponent;
  let fixture: ComponentFixture<BreadcrumbLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [ BreadcrumbLinkComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreadcrumbLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
