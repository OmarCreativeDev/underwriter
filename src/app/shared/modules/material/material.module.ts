import { NgModule } from '@angular/core';
import {
  MAT_DATE_LOCALE,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatTableModule,
  MatTooltipModule,
} from '@angular/material';

import { CdkTableModule } from '@angular/cdk/table';

const modules: Array<Object> = [
  CdkTableModule,
  MatDatepickerModule,
  MatDialogModule,
  MatFormFieldModule,
  MatInputModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatTableModule,
  MatTooltipModule,
];

@NgModule({
  imports: [modules],
  exports: [modules],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
  ]
})
export class MaterialModule {}
