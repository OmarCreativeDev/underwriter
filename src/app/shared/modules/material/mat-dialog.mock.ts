import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MatDialogMock {

  public open(): void {}
  public close(): void {}

}
