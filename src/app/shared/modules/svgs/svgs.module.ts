import { BlueRightArrowSvgComponent } from '../../components/svgs/blue-right-arrow-svg/blue-right-arrow-svg.component';
import { CommonModule } from '@angular/common';
import { DownArrowSvgComponent } from '../../components/svgs/down-arrow-svg/down-arrow-svg.component';
import { DownloadSvgComponent } from '../../components/svgs/download-svg/download-svg.component';
import { ExperianLogoSvgComponent } from '../../components/svgs/experian-logo-svg/experian-logo-svg.component';
import { LeftArrowSvgComponent } from '../../components/svgs/left-arrow-svg/left-arrow-svg.component';
import { LogoutSvgComponent } from '../../components/svgs/logout-svg/logout-svg.component';
import { NgModule } from '@angular/core';
import { PinkRightArrowSvgComponent } from '../../components/svgs/pink-right-arrow-svg/pink-right-arrow-svg.component';
import { SearchSvgComponent } from '../../components/svgs/search-svg/search-svg.component';
import { UpArrowSvgComponent } from '../../components/svgs/up-arrow-svg/up-arrow-svg.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BlueRightArrowSvgComponent,
    DownArrowSvgComponent,
    DownloadSvgComponent,
    ExperianLogoSvgComponent,
    LeftArrowSvgComponent,
    PinkRightArrowSvgComponent,
    SearchSvgComponent,
    UpArrowSvgComponent,
    LogoutSvgComponent,
  ],
  exports: [
    BlueRightArrowSvgComponent,
    DownArrowSvgComponent,
    DownloadSvgComponent,
    ExperianLogoSvgComponent,
    LeftArrowSvgComponent,
    LogoutSvgComponent,
    PinkRightArrowSvgComponent,
    SearchSvgComponent,
    UpArrowSvgComponent,
  ],
})
export class SvgsModule {}
