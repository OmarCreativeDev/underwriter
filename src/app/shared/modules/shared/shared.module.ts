import { AppRoutingModule } from '../../../app-routing.module';
import { BreadcrumbLinkComponent } from '../../components/breadcrumb-link/breadcrumb-link.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts-x';
import { CommonModule, CurrencyPipe, DatePipe } from '@angular/common';
import { DownloadAsDataBtnComponent } from '../../components/download-as-data-btn/download-as-data-btn.component';
import { DownloadAsPdfDialogComponent } from '../../components/download-as-pdf-dialog/download-as-pdf-dialog.component';
import { FooterComponent } from '../../components/footer/footer.component';
import { FormattedSortCodePipe } from '../../pipes/formatted-sort-code/formatted-sort-code.pipe';
import { HeaderComponent } from '../../components/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { LogoutBtnComponent } from '../../components/logout-btn/logout-btn.component';
import { MaterialModule } from '../material/material.module';
import { NgModule } from '@angular/core';
import { OrderByPipe } from '../../pipes/order-by/order-by.pipe';
import { ReactiveFormsModule } from '@angular/forms';
import { SvgsModule } from '../svgs/svgs.module';

const componentsAndPipes: Array<Object> = [
  BreadcrumbLinkComponent,
  DownloadAsDataBtnComponent,
  DownloadAsPdfDialogComponent,
  FooterComponent,
  FormattedSortCodePipe,
  HeaderComponent,
  LogoutBtnComponent,
  OrderByPipe,
];

@NgModule({
  imports: [
    AppRoutingModule,
    ChartsModule,
    CommonModule,
    HttpClientModule,
    MaterialModule,
    ReactiveFormsModule,
    SvgsModule,
  ],
  declarations: [ componentsAndPipes ],
  providers: [OrderByPipe, DatePipe, CurrencyPipe],
  exports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    ChartsModule,
    componentsAndPipes,
    MaterialModule,
    ReactiveFormsModule,
    SvgsModule,
  ],
  entryComponents: [DownloadAsPdfDialogComponent]
})
export class SharedModule {}
