import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipeMock implements PipeTransform {
  transform(): any {
    return [];
  }
}
