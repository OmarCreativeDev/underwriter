import { Pipe, PipeTransform } from '@angular/core';
import { orderBy } from 'lodash';

@Pipe({
  name: 'orderBy'
})
export class OrderByPipe implements PipeTransform {
  transform(list: Array<any>, property: Array<string> | string, direction: Array<string> | string): Array<any> {
    return orderBy(list, property, direction);
  }
}
