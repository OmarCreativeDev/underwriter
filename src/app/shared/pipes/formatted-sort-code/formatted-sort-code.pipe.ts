import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formattedSortCode'
})
export class FormattedSortCodePipe implements PipeTransform {

  transform(value: string): string {
    let formattedSortCode: string;

    if (value) {
      // grab first, middle & last 2 string characters
      const firstSlice: string = value.slice(0, 2);
      const middleSlice: string = value.slice(2, 4);
      const lastSlice: string = value.slice(4);

      // join slices and seperate with hyphen
      formattedSortCode = `${firstSlice}-${middleSlice}-${lastSlice}`;
    }

    return formattedSortCode;
  }

}
