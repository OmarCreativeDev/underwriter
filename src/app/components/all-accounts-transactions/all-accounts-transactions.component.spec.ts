import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteMock } from '../../services/activated-route.mock';
import { ApplicationService } from '../../services/application/application.service';
import { ApplicationServiceMock } from '../../services/application/application.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MatDatepickerModule, MatNativeDateModule, MatTooltipModule } from '@angular/material';
import { AllAccountsTransactionsComponent } from './all-accounts-transactions.component';
import { of } from 'rxjs';
import { OrderByPipe } from '../../shared/pipes/order-by/order-by.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { transactionMock, TransactionServiceMock } from '../../services/transaction/transaction.service.mock';
import { TransactionService } from '../../services/transaction/transaction.service';

describe('AllAccountsTransactionsComponent', () => {
  let component: AllAccountsTransactionsComponent;
  let fixture: ComponentFixture<AllAccountsTransactionsComponent>;
  let transactionService: TransactionService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OrderByPipe,
        AllAccountsTransactionsComponent,
      ],
      imports: [
        MatDatepickerModule,
        MatNativeDateModule,
        MatTooltipModule,
        RouterTestingModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteMock },
        { provide: ApplicationService, useClass: ApplicationServiceMock },
        { provide: TransactionService, useClass: TransactionServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllAccountsTransactionsComponent);
    component = fixture.componentInstance;
    transactionService = TestBed.get(TransactionService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` should invoke `getPartyId()`', () => {
    spyOn(component, 'getPartyId');
    component.ngOnInit();
    expect(component.getPartyId).toHaveBeenCalled();
  });

  it('`getPartyId()` sets `partyId` and invokes `getSubCategory()`', () => {
    spyOn(component, 'getSubCategory');
    component.getPartyId();
    expect(component.partyId).toBeDefined();
    expect(component.partyId).toEqual(123123);
    expect(component.getSubCategory).toHaveBeenCalled();
  });

  it('`getSubCategory()` sets `subCategory` and invokes `listAllTransactionsByFilter()`', () => {
    spyOn(component, 'listAllTransactionsByFilter');
    component.getSubCategory();
    expect(component.subCategory).toBeDefined();
    expect(component.subCategory).toEqual('Entertainment');
    expect(component.listAllTransactionsByFilter).toHaveBeenCalled();
  });

  it('`listAllTransactionsByFilter()` makes api call and on success sets `loading` and data properties', () => {
    spyOn(transactionService, 'listAllTransactionsByFilter').and.returnValue(of([transactionMock]));

    component.listAllTransactionsByFilter(33333);

    expect(transactionService.listAllTransactionsByFilter).toHaveBeenCalled();
    expect(component.loading).toBeFalsy();
    expect(component.allTransactions).toBeDefined();
    expect(component.allTransactions.length).toEqual(1);
    expect(component.transactions).toBeDefined();
    expect(component.transactions.length).toEqual(1);
  });

  it('`setFromDate()` should set `fromDate` and `minDate`', () => {
    const mockDateString: string = '10/11/2019';
    const mockDate: moment.Moment = moment(mockDateString, 'DD/MM/YYYY');
    component.setFromDate(mockDateString);

    expect(component.fromDate).toBeDefined();
    expect(component.fromDate).toEqual(mockDate);
    expect(component.minDate).toBeDefined();
    expect(component.minDate).toEqual(mockDate);
  });

  it('`setToDate()` should set `toDate` and invoke `filterByDateRange()`', () => {
    spyOn(component, 'filterByDateRange');

    const mockDateString: string = '03/12/2019';
    const mockDate: moment.Moment = moment(mockDateString, 'DD/MM/YYYY');
    component.setToDate(mockDateString);

    expect(component.toDate).toBeDefined();
    expect(component.toDate).toEqual(moment(mockDate));
    expect(component.filterByDateRange).toHaveBeenCalled();
  });

  it('`clearDateRange()` should clear all relevant fields', () => {
    component.clearDateRange();
    expect(component.fromInput.value).toEqual('');
    expect(component.toInput.value).toEqual('');
    expect(component.transactions).toEqual(component.allTransactions);
  });
});
