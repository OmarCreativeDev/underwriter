import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../services/application/application.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ITransaction, allAccountsTransactionsMap } from '../../services/transaction/transaction.interface';
import { MatInput } from '@angular/material';
import { roundedFiguresMsg } from '../../services/finance/finance.interface';
import { first } from 'rxjs/operators';
import { TransactionService } from '../../services/transaction/transaction.service';

@Component({
  selector: 'app-all-accounts-transactions',
  templateUrl: './all-accounts-transactions.component.html',
  styleUrls: ['./all-accounts-transactions.component.scss'],
})
export class AllAccountsTransactionsComponent implements OnInit {

  public allTransactions: Array<ITransaction> = [];
  public error: string;
  public fromDate: moment.Moment;
  public loading: boolean = true;
  public minDate: moment.Moment;
  public partyId: number;
  public roundedFiguresMsg: string = roundedFiguresMsg;
  public startDate: string = moment().format();
  public subCategory: string;
  public toDate: moment.Moment;
  public transactions: Array<ITransaction> = [];
  public allAccountsTransactionsMap: Array<Object> = allAccountsTransactionsMap;

  @ViewChild('fromInput') public fromInput: MatInput;
  @ViewChild('toInput') public toInput: MatInput;

  constructor(
    private activatedRoute: ActivatedRoute,
    private applicationService: ApplicationService,
    private transactionService: TransactionService,
  ) {}

  ngOnInit() {
    this.getPartyId();
  }

  public getPartyId(): void {
    this.applicationService.partyIdStream
      .pipe(first())
      .subscribe((partyId: number) => {
        this.partyId = partyId;
        this.getSubCategory();
      });
  }

  public getSubCategory(): void {
    this.activatedRoute.params
      .pipe(first())
      .subscribe((params: Object) => {
        this.subCategory = params['filter'] || null;
        this.listAllTransactionsByFilter(this.partyId, this.subCategory);
      });
  }

  public listAllTransactionsByFilter(partyId: number, subCategory?: string): void {
    this.transactionService.listAllTransactionsByFilter(partyId, subCategory)
      .pipe(first())
      .subscribe((modifiedTransactions: Array<ITransaction>) => {
        this.loading = false;
        this.allTransactions = modifiedTransactions;
        this.transactions = modifiedTransactions;
      },
      (httpErrorResponse: HttpErrorResponse) => {
        this.loading = false;
        this.error = `${httpErrorResponse.statusText}, code: ${httpErrorResponse.status}`;
      });
  }

  public setFromDate(date: string): void {
    this.fromDate = moment(date, 'DD/MM/YYYY');
    this.minDate = this.fromDate;
  }

  public setToDate(date: string): void {
    this.toDate = moment(date, 'DD/MM/YYYY');
    this.filterByDateRange();
  }

  public filterByDateRange(): void {
    if (this.toDate.isValid() && this.fromDate.isValid()) {
      this.transactions = this.allTransactions.filter((item: ITransaction) => {
        const transactionDate: moment.Moment = moment(item.date);
        const withinFromDateRange: boolean = transactionDate >= this.fromDate;
        const withinToDateRange: boolean = transactionDate <= this.toDate;

        if (withinFromDateRange && withinToDateRange) {
          return item;
        }
      });
    }
  }

  public clearDateRange(): void {
    this.fromInput.value = '';
    this.toInput.value = '';
    this.transactions = this.allTransactions;
  }

}
