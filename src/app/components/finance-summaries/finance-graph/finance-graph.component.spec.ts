import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FinanceGraphComponent } from './finance-graph.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FinanceService } from '../../../services/finance/finance.service';
import { financeMock, FinanceServiceMock } from '../../../services/finance/finance.service.mock';
import { ChartsModule } from 'ng2-charts-x';

describe('FinanceGraphComponent', () => {
  let component: FinanceGraphComponent;
  let fixture: ComponentFixture<FinanceGraphComponent>;
  let financeService: FinanceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceGraphComponent ],
      imports: [ ChartsModule ],
      providers: [
        { provide: FinanceService, useClass: FinanceServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceGraphComponent);
    component = fixture.componentInstance;
    financeService = TestBed.get(FinanceService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`hexColors` should be an array with 6 members', () => {
    expect(Array.isArray(component.hexColors)).toBeTruthy();
    expect(component.hexColors.length).toEqual(6);
  });

  it('`chartType` should equal `line`', () => {
    expect(component.chartType).toEqual('line');
  });

  it('`ngOnInit()` should invoke 3 functions', () => {
    spyOn(component, 'setOptionsConfig');
    spyOn(component, 'setColorsConfig');
    spyOn(component, 'getSummaries');

    component.ngOnInit();
    expect(component.chartType).toEqual('line');
  });

  it('`showDetailedGraph()` should set `isDetailedGraph` to true and invokes `handleDataSets()`', () => {
    spyOn(component, 'handleDataSets');
    component.showDetailedGraph();
    expect(component.isDetailedGraph).toBeTruthy();
    expect(component.handleDataSets).toHaveBeenCalled();
  });

  it('`showSimpleGraph()` should set `isDetailedGraph` to false and invokes `handleDataSets()`', () => {
    spyOn(component, 'handleDataSets');
    component.showSimpleGraph();
    expect(component.isDetailedGraph).toBeFalsy();
    expect(component.handleDataSets).toHaveBeenCalled();
  });

  it('`setOptionsConfig()` should set graph options', () => {
    component.setOptionsConfig();
    expect(component.options.hasOwnProperty('legend')).toBeTruthy();
    expect(component.options['legend'].position).toEqual('bottom');
    expect(component.options.hasOwnProperty('scales')).toBeTruthy();
  });

  it('`setColorsConfig()` should set graph colors config', () => {
    component.colors = [];
    component.setColorsConfig();
    expect(component.colors.length).toEqual(2);
  });

  it('`setSummariesStream()` should return correct data stream', () => {
    const dataStream: string = component.setSummariesStream();
    expect(dataStream).toEqual('affordabilitySummariesStream');
  });

  it('`getSummaries()` should set summaries data and invoke `handleDataSets()`', () => {
    spyOn(component, 'handleDataSets');
    financeService.affordabilitySummariesStream.next([financeMock]);
    component.getSummaries();

    expect(component.incomeSummaries.length).toEqual(1);
    expect(component.expenditureSummaries.length).toEqual(0);
    expect(component.handleDataSets).toHaveBeenCalled();
  });
});
