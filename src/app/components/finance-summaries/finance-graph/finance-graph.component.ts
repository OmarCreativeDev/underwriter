import { IFinanceMonth, IFinanceRecord } from '../../../services/finance/finance.interface';
import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FinanceService } from '../../../services/finance/finance.service';
import { HttpErrorResponse } from '@angular/common/http';
import { map, orderBy } from 'lodash';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-finance-graph',
  templateUrl: './finance-graph.component.html',
  styleUrls: ['./finance-graph.component.scss']
})
export class FinanceGraphComponent implements OnInit, OnDestroy {

  @Input() public isDetailedGraph: boolean = false;
  @Input() public isBankAccountSummary: boolean = false;
  @Input() public showToggle: boolean = false;

  public chartType: string = 'line';
  public colors: Array<Object> = [];
  public componentAlive: boolean = true;
  public datasets: Array<Object>;
  public error: string;
  public expenditureSummaries: Array<IFinanceRecord>;
  public hexColors: Array<string> = ['#94077d', '#26478d', '#9b9b9b', '#dd9218', '#4a90e2', '#6fac46'];
  public incomeSummaries: Array<IFinanceRecord>;
  public labels: Array<string> = [];
  public loading: boolean = true;
  public options: Object = {};

  constructor(
    private financeService: FinanceService,
  ) {}

  ngOnInit(): void {
    this.setOptionsConfig();
    this.setColorsConfig();
    this.getSummaries();
  }

  /**
   * Redraw chart ui as detailed graph
   */
  public showDetailedGraph(): void {
    this.isDetailedGraph = true;
    this.handleDataSets();
  }

  /**
   * Redraw chart ui as simple graph
   */
  public showSimpleGraph(): void {
    this.isDetailedGraph = false;
    this.handleDataSets();
  }

  /**
   * Set chartsJs options
   */
  public setOptionsConfig(): void {
    this.options = {
      legend: {
        position: 'bottom',
        labels: {
          fontColor: this.hexColors[1],
          usePointStyle: true,
          padding: 40,
        }
      },
      scales: {
        yAxes: [{
          ticks: {
            callback: label => `£${label}`
          }
        }]
      }
    };
  }

  /**
   * Set chartJs colors config
   */
  public setColorsConfig(): void {
    const legendsCount: number = this.isDetailedGraph ? 6 : 2;

    for (let i = 0; i < legendsCount; i++) {
      this.colors.push(
        {
          backgroundColor: 'transparent',
          borderColor: this.hexColors[i],
          pointBackgroundColor: this.hexColors[i],
          pointBorderColor: this.hexColors[i],
          pointHoverBackgroundColor: this.hexColors[i],
          pointHoverBorderColor: this.hexColors[i],
        }
      );
    }
  }

  public setSummariesStream(): string {
    return this.isBankAccountSummary ? 'bankAccountSummariesStream' : 'affordabilitySummariesStream';
  }

  /**
   * Subscribe to summary stream
   * And grab finance summary data
   * Then invoke method to generate graph data
   */
  public getSummaries(): void {
    this.financeService[this.setSummariesStream()]
      .pipe(takeWhile(() => this.componentAlive))
      .subscribe((stream: Array<IFinanceRecord> | HttpErrorResponse) => {
        if (stream) {
          if (Array.isArray(stream)) {
            this.incomeSummaries = stream.filter((item: IFinanceRecord) => item.isIncome);
            this.expenditureSummaries = stream.filter((item: IFinanceRecord) => !item.isIncome);
            if (this.incomeSummaries[0].data) {
              this.labels = this.financeService.generateMonths(this.incomeSummaries[0].data);
            }
            this.handleDataSets();
          } else {
            this.loading = false;
            this.error = `${stream.statusText}, code: ${stream.status}`;
          }
        }
      });
  }

  public handleDataSets(): void {
    if (this.isDetailedGraph) {
      this.setDetailedDataset(this.incomeSummaries, this.expenditureSummaries);
    } else {
      this.setSimpleDatasets(
        this.createSimpleDataset(this.incomeSummaries),
        this.createSimpleDataset(this.expenditureSummaries),
      );
    }

    this.loading = false;
  }

  private setDetailedDataset(detailedIncome: Array<IFinanceRecord>, detailedExpenditure: Array<IFinanceRecord>): void {
    let detailedDataSets: Array<IFinanceRecord> = [...detailedIncome, ...detailedExpenditure];
    detailedDataSets = orderBy(detailedDataSets, 'code');

    this.datasets = [];

    detailedDataSets.forEach((item: IFinanceRecord) => {
      const months: Array<number> = [];

      item.data.forEach((month: IFinanceMonth) => {
        months.push(month.totalAmount);
      });

      this.datasets.push({
        label: item.metric,
        data: months,
      });
    });
  }

  /**
   * Receive financeSummaries data
   * Then generate and return graph friendly data
   * For showing simple graph
   * @param financeSummaries
   */
  private createSimpleDataset(financeSummaries: Array<IFinanceRecord>): Array<number> {
    const annualFinances: Array<Array<number>> = [];

    // collect last 12 months of finance amounts of each finance type
    financeSummaries.forEach((item: IFinanceRecord) => {
      annualFinances.push(map(item.data, 'totalAmount'));
    });

    // allocate first annual finance amounts
    const combinedSummary: Array<number> = annualFinances.shift();

    // combine all annual finance amounts
    annualFinances.forEach((month: Array<number>) => {
      month.forEach((value: number, index: number) => {
        combinedSummary[index] += value;
      });
    });

    return combinedSummary;
  }

  /**
   * Receive graphy friendly data
   * Then set to graph dataset
   * In order to display simple graph ui
   * @param combinedIncome
   * @param combinedExpenditure
   */
  private setSimpleDatasets(combinedIncome: Array<number>, combinedExpenditure: Array<number>): void {
    this.datasets = [
      { data: combinedIncome, label: 'Income' },
      { data: combinedExpenditure, label: 'Expenditure' },
    ];
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
