import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FinanceTableHeaderComponent } from './finance-table-header.component';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('FinanceTableHeaderComponent', () => {
  let component: FinanceTableHeaderComponent;
  let fixture: ComponentFixture<FinanceTableHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceTableHeaderComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceTableHeaderComponent);
    component = fixture.componentInstance;
    component.months = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
