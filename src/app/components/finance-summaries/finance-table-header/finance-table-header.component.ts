import { Component, Input } from '@angular/core';
import { IFinanceMonth } from '../../../services/finance/finance.interface';

@Component({
  selector: '[app-finance-table-header]',
  templateUrl: './finance-table-header.component.html',
})
export class FinanceTableHeaderComponent {

  @Input() public months: Array<IFinanceMonth>;

}
