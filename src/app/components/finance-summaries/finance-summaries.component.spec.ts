import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteMock } from '../../services/activated-route.mock';
import { ApplicationService } from '../../services/application/application.service';
import { ApplicationServiceMock } from '../../services/application/application.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FinanceService } from '../../services/finance/finance.service';
import { FinanceServiceMock } from '../../services/finance/finance.service.mock';
import { FinanceSummariesComponent } from './finance-summaries.component';
import { FinanceTableRecordComponent } from './finance-table-record/finance-table-record.component';
import { OrderByPipe } from '../../shared/pipes/order-by/order-by.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { FinanceTableHeaderComponent } from './finance-table-header/finance-table-header.component';

describe('FinanceSummariesComponent', () => {
  let component: FinanceSummariesComponent;
  let fixture: ComponentFixture<FinanceSummariesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FinanceSummariesComponent,
        FinanceTableRecordComponent,
        FinanceTableHeaderComponent,
        OrderByPipe,
      ],
      imports: [ RouterTestingModule ],
      providers: [
        { provide: FinanceService, useClass: FinanceServiceMock },
        { provide: ActivatedRoute, useClass: ActivatedRouteMock },
        { provide: ApplicationService, useClass: ApplicationServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceSummariesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
