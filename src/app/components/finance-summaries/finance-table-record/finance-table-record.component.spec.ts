import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FinanceService } from '../../../services/finance/finance.service';
import { financeMock, FinanceServiceMock } from '../../../services/finance/finance.service.mock';
import { FinanceTableRecordComponent } from './finance-table-record.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('FinanceTableRecordComponent', () => {
  let component: FinanceTableRecordComponent;
  let fixture: ComponentFixture<FinanceTableRecordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceTableRecordComponent ],
      imports: [ RouterTestingModule ],
      providers: [
        { provide: FinanceService, useClass: FinanceServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceTableRecordComponent);
    component = fixture.componentInstance;
    component.data = financeMock;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
