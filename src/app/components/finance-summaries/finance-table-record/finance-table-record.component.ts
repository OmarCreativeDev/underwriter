import { Component, Input } from '@angular/core';
import { IFinanceRecord } from '../../../services/finance/finance.interface';

@Component({
  selector: '[app-finance-table-record]',
  templateUrl: './finance-table-record.component.html',
  styleUrls: ['./finance-table-record.component.scss'],
})
export class FinanceTableRecordComponent {

  @Input() public data: IFinanceRecord;
  @Input() public isSummaryDetails: boolean;
  @Input() public partyId: string;

  /**
   * Replace all '/' in category name with a space
   * So that the ui doesnt break in layout
   */
  public formatSubCategory(category: string): string {
    let transformedCategory: string = category;

    if (transformedCategory.includes('/')) {
      transformedCategory = transformedCategory.replace(/\//g, ' / ');
    }

    return transformedCategory;
  }

}
