import { ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../services/application/application.service';
import { Component, Input, OnInit } from '@angular/core';
import { FinanceService } from '../../services/finance/finance.service';
import { first } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { IFinanceRecord, roundedFiguresMsg } from '../../services/finance/finance.interface';

@Component({
  selector: 'app-finance-summaries',
  templateUrl: './finance-summaries.component.html',
})
export class FinanceSummariesComponent implements OnInit {

  public error: string;
  public expenditureSummaries: Array<IFinanceRecord>;
  public incomeSummaries: Array<IFinanceRecord>;
  public loading: boolean = true;
  public partyId: number;
  public roundedFiguresMsg: string = roundedFiguresMsg;
  @Input() public ownedItemId: number;

  constructor(
    private financeService: FinanceService,
    private activatedRoute: ActivatedRoute,
    private applicationService: ApplicationService,
  ) {}

  ngOnInit(): void {
    this.getPartyId();
  }

  private getPartyId(): void {
    this.applicationService.partyIdStream
      .pipe(first())
      .subscribe((partyId: number) => {
        this.partyId = partyId;
        this.handleSummariesType(partyId);
      });
  }

  private handleSummariesType(partyId: number): void {
    this.setupListener();

    if (this.ownedItemId) {
      this.listBankAccountSummaries(partyId, this.ownedItemId);
    } else {
      this.listAffordabilitySummaries(partyId);
    }
  }

  public listBankAccountSummaries(partyId: number, ownedItemId: number): void {
    this.financeService.listBankAccountSummaries(partyId, ownedItemId)
      .pipe(first())
      .subscribe((data: Array<IFinanceRecord>) => {
        this.financeService.bankAccountSummariesStream.next(data);
      },
      (httpErrorResponse: HttpErrorResponse) => {
        this.financeService.bankAccountSummariesStream.next(httpErrorResponse);
      });
  }

  public listAffordabilitySummaries(partyId: number): void {
    this.financeService.listAffordabilitySummaries(partyId)
      .pipe(first())
      .subscribe((data: Array<IFinanceRecord>) => {
        this.financeService.affordabilitySummariesStream.next(data);
      },
      (httpErrorResponse: HttpErrorResponse) => {
        this.financeService.affordabilitySummariesStream.next(httpErrorResponse);
      });
  }

  private setSummariesStream(): string {
    return this.ownedItemId ? 'bankAccountSummariesStream' : 'affordabilitySummariesStream';
  }

  private setupListener(): void {
    this.financeService[this.setSummariesStream()]
      .pipe(first())
      .subscribe((stream: Array<IFinanceRecord> | HttpErrorResponse) => {
        if (stream) {
          this.loading = false;

          if (Array.isArray(stream)) {
            this.incomeSummaries = stream.filter((item: IFinanceRecord) => item.isIncome);
            this.expenditureSummaries = stream.filter((item: IFinanceRecord) => !item.isIncome);
          } else {
            this.error = `${stream.statusText}, code: ${stream.status}`;
          }
        }
      });
  }

  private setFinanceDetailsApi(): string {
    return this.ownedItemId ? 'getBankAccountDetails' : 'getAffordabilityDetails';
  }

  public showSummaryDetails(financeName: string, categoryCode: number, isIncome: boolean): void {
    // calculate index of finance record
    const index: number = this.financeSummaryIndex(financeName, categoryCode);

    // check whether details data is present
    const hasDetails: boolean = this[financeName][index].subTransactionsCategorySummaries.length ? true : false;

    // if details data is missing make api call to fetch it
    if (!hasDetails) {
      this[this.setFinanceDetailsApi()](financeName, categoryCode, isIncome, index);
    }

    // expand / collapse details
    this[financeName][index].showDetails = !this[financeName][index].showDetails;
  }

  /**
   * Make server side api call
   * And grab affordability details
   * @param financeName
   * @param categoryCode
   * @param isIncome
   * @param index
   */
  private getAffordabilityDetails(financeName: string, categoryCode: number, isIncome: boolean, index: number): void {
    this.financeService.getAffordabilityDetails(this.partyId, categoryCode, isIncome)
      .pipe(first())
      .subscribe((data: Array<IFinanceRecord>) => {
        this[financeName][index].subTransactionsCategorySummaries = data;
      });
  }

  /**
   * Make server side api call
   * And grab bank account details
   * @param financeName
   * @param categoryCode
   * @param isIncome
   * @param index
   */
  private getBankAccountDetails(financeName: string, categoryCode: number, isIncome: boolean, index: number): void {
    this.financeService.getBankAccountDetails(this.partyId, this.ownedItemId, categoryCode, isIncome)
      .pipe(first())
      .subscribe((data: Array<IFinanceRecord>) => {
        this[financeName][index].subTransactionsCategorySummaries = data;
      });
  }

  /**
   * Filter through summaries
   * And return current index based on metric
   * @param financeName
   * @param order
   */
  public financeSummaryIndex(financeName: string, order: number): number {
    return this[financeName].findIndex((summary: IFinanceRecord) => {
      return summary.order === order;
    });
  }

}
