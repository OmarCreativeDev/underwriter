import { applicationColumns, applicationLabels, IApplication } from '../../services/application/application.interface';
import { ApplicationService } from '../../services/application/application.service';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.scss'],
})
export class ApplicationDetailsComponent implements OnInit {

  public application: IApplication;
  public columns: Array<string> = applicationColumns;
  public error: string;
  public labels: Array<string> = applicationLabels;
  public loading: boolean = true;

  constructor(
    private applicationService: ApplicationService,
  ) {}

  ngOnInit(): void {
    this.getPartyId();
  }

  public getPartyId(): void {
    this.applicationService.partyIdStream
      .pipe(first())
      .subscribe((partyId: number) => {
        this.getApplication(partyId);
      });
  }

  public getApplication(partyId: number): void {
    this.applicationService.get(partyId)
      .pipe(first())
      .subscribe((data: IApplication) => {
        this.loading = false;
        this.application = data;
      },
      (httpErrorResponse: HttpErrorResponse) => {
        this.loading = false;
        this.error = `${httpErrorResponse.statusText}, code: ${httpErrorResponse.status}`;
      });
  }

}
