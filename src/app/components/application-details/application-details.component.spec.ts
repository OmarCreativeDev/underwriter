import { ApplicationDetailsComponent } from './application-details.component';
import { ApplicationService } from '../../services/application/application.service';
import { ApplicationServiceMock } from '../../services/application/application.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

describe('ApplicationDetailsComponent', () => {
  let component: ApplicationDetailsComponent;
  let fixture: ComponentFixture<ApplicationDetailsComponent>;
  let applicationService: ApplicationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationDetailsComponent ],
      providers: [
        { provide: ApplicationService, useClass: ApplicationServiceMock }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationDetailsComponent);
    component = fixture.componentInstance;
    applicationService = TestBed.get(ApplicationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` invokes `getPartyId()`', () => {
    spyOn(component, 'getPartyId');
    component.ngOnInit();
    expect(component.getPartyId).toHaveBeenCalled();
  });

  it('`getPartyId()` invokes `getApplication()`', () => {
    spyOn(component, 'getApplication');
    component.getPartyId();
    expect(component.getApplication).toHaveBeenCalledWith(123123);
  });

  it('`getApplication()` sets `loading` and `application` if api call is successful', () => {
    component.getApplication(123123);
    expect(component.loading).toBeFalsy();
    expect(component.application).toBeDefined();
  });

  it('`getApplication()` sets `loading` and `error` if api call fails', () => {
    spyOn(applicationService, 'get').and.returnValue(
      throwError(new HttpErrorResponse({ statusText: 'Api call failed', status: 400 }))
    );
    component.getApplication(123123);
    expect(component.loading).toBeFalsy();
    expect(component.error).toBeDefined();
    expect(component.error).toEqual('Api call failed, code: 400');
  });
});
