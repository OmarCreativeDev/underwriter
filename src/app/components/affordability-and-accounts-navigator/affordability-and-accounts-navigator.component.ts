import { Component } from '@angular/core';

@Component({
  selector: 'app-affordability-and-accounts-navigator',
  templateUrl: './affordability-and-accounts-navigator.component.html',
  styleUrls: ['./affordability-and-accounts-navigator.component.scss']
})
export class AffordabilityAndAccountsNavigatorComponent {}
