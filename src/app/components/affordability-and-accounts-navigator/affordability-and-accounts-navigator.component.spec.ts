import { AffordabilityAndAccountsNavigatorComponent } from './affordability-and-accounts-navigator.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

describe('AffordabilityAndAccountsNavigatorComponent', () => {
  let component: AffordabilityAndAccountsNavigatorComponent;
  let fixture: ComponentFixture<AffordabilityAndAccountsNavigatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [ AffordabilityAndAccountsNavigatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffordabilityAndAccountsNavigatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
