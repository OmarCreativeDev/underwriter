import { Component, Input, OnChanges, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { applicationColumns, applicationLabels, IApplication } from '../../services/application/application.interface';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ApplicationService } from '../../services/application/application.service';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-applications-list',
  templateUrl: './applications-list.component.html',
  styleUrls: ['./applications-list.component.scss']
})
export class ApplicationsListComponent implements OnInit, OnChanges, OnDestroy {

  @Input() public applications: Array<IApplication>;
  private componentAlive: boolean = true;
  public dataSource: MatTableDataSource<IApplication>;
  public labels: Array<string> = applicationLabels;
  public columns: Array<string> = applicationColumns;

  @ViewChild(MatPaginator) public paginator: MatPaginator;

  constructor(
    private applicationService: ApplicationService,
  ) {}

  ngOnChanges(): void {
    this.dataSource = new MatTableDataSource<IApplication>(this.applications);
  }

  ngOnInit(): void {
    if (this.dataSource) {
      this.dataSource.paginator = this.paginator;
    }
    this.setupListener();
  }

  private setupListener(): void {
    this.applicationService.filterEvent
      .pipe(takeWhile(() => this.componentAlive))
      .subscribe((filterValue: string) => {
        this.dataSource.filter = filterValue;
      });
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
