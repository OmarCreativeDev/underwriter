import { ApplicationService } from '../../services/application/application.service';
import { ApplicationServiceMock } from '../../services/application/application.service.mock';
import { ApplicationsListComponent } from './applications-list.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { MaterialModule } from '../../shared/modules/material/material.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('ApplicationsListComponent', () => {
  let component: ApplicationsListComponent;
  let fixture: ComponentFixture<ApplicationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MaterialModule,
        BrowserAnimationsModule,
      ],
      declarations: [ ApplicationsListComponent ],
      providers: [
        { provide: ApplicationService, useClass: ApplicationServiceMock }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsListComponent);
    component = fixture.componentInstance;
    component.applications = [{
      createdOn: '2018-11-09T14:50:11.4817679',
      dateOfBirth: '1973-01-03T00:00:00',
      dateOfBirthFormatted: '03/01/1973',
      name: 'russell berna',
      partyID: 488317,
      postcode: 'B64 7EQ',
      referenceNumber: 'ABC 12345',
      updatedOn: '2018-11-09T14:57:43.5873445',
    }];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
