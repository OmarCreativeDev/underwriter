import { AccountOverviewComponent } from './account-overview.component';
import { AccountService } from '../../../services/account/account.service';
import { AccountServiceMock } from '../../../services/account/account.service.mock';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteMock } from '../../../services/activated-route.mock';
import { ApplicationService } from '../../../services/application/application.service';
import { ApplicationServiceMock } from '../../../services/application/application.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormattedSortCodePipe } from '../../../shared/pipes/formatted-sort-code/formatted-sort-code.pipe';

describe('AccountOverviewComponent', () => {
  let component: AccountOverviewComponent;
  let fixture: ComponentFixture<AccountOverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AccountOverviewComponent,
        FormattedSortCodePipe,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteMock },
        { provide: AccountService, useClass: AccountServiceMock },
        { provide: ApplicationService, useClass: ApplicationServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` invokes `getPartyId()`', () => {
    spyOn(component, 'getPartyId');
    component.ngOnInit();
    expect(component.getPartyId).toHaveBeenCalled();
  });

  it('`getPartyId()` invokes `getOwnedItemId()`', () => {
    spyOn(component, 'getOwnedItemId');
    component.getPartyId();
    expect(component.getOwnedItemId).toHaveBeenCalledWith(123123);
  });

  it('`getOwnedItemId()` invokes `getAccount()`', () => {
    spyOn(component, 'getAccount');
    component.getOwnedItemId(123123);
    expect(component.getAccount).toHaveBeenCalledWith(123123, 67676);
  });

  it('`getAccount()` sets `loading` and `account`', () => {
    component.getAccount(565656, 898989);
    expect(component.loading).toBeFalsy();
    expect(component.account).toBeDefined();
  });
});
