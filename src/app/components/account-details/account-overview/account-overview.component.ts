import { Component, OnInit } from '@angular/core';
import { accountColumns, accountLabels, IAccount } from '../../../services/account/account.interface';
import { AccountService } from '../../../services/account/account.service';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { ApplicationService } from '../../../services/application/application.service';

@Component({
  selector: 'app-account-overview',
  templateUrl: './account-overview.component.html',
  styleUrls: ['./account-overview.component.scss']
})
export class AccountOverviewComponent implements OnInit {

  public loading: boolean = true;
  public account: IAccount;
  public labels: Array<string> = accountLabels;
  public columns: Array<string> = accountColumns;
  public error: string;

  constructor(
    private accountService: AccountService,
    private activatedRoute: ActivatedRoute,
    private applicationService: ApplicationService,
  ) {}

  ngOnInit(): void {
    this.getPartyId();
  }

  public getPartyId(): void {
    this.applicationService.partyIdStream
      .pipe(first())
      .subscribe((partyId: number) => {
        this.getOwnedItemId(partyId);
      });
  }

  public getOwnedItemId(partyId: number): void {
    this.activatedRoute.params
      .pipe(first())
      .subscribe((data: Object) => {
        this.getAccount(partyId, data['ownedItemId']);
      });
  }

  public getAccount(partyId: number, ownedItemId: number): void {
    this.accountService.get(partyId, ownedItemId)
      .pipe(first())
      .subscribe((data: IAccount) => {
        this.loading = false;
        this.account = data;
      },
      (httpErrorResponse: HttpErrorResponse) => {
        this.loading = false;
        this.error = this.accountService.errorHandling(httpErrorResponse);
      });
  }

}
