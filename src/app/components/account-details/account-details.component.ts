import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
})
export class AccountDetailsComponent implements OnInit {

  public ownedItemId: number;

  constructor(
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.getOwnedItemId();
  }

  public getOwnedItemId(): void {
    this.activatedRoute.params
      .pipe(first())
      .subscribe((data: Object) => {
        this.ownedItemId = data['ownedItemId'];
      });
  }

}
