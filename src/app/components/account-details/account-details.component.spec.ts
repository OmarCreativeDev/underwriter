import { AccountDetailsComponent } from './account-details.component';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteMock } from '../../services/activated-route.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('AccountDetailsComponent', () => {
  let component: AccountDetailsComponent;
  let fixture: ComponentFixture<AccountDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDetailsComponent ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` invokes `getOwnedItemId()`', () => {
    spyOn(component, 'getOwnedItemId');
    component.ngOnInit();
    expect(component.getOwnedItemId).toHaveBeenCalled();
  });

  it('`getOwnedItemId()` sets `ownedItemId`', () => {
    component.getOwnedItemId();
    expect(component.ownedItemId).toBeDefined();
    expect(component.ownedItemId).toEqual(67676);
  });
});
