import { ApplicationService } from '../../services/application/application.service';
import { ApplicationServiceMock } from '../../services/application/application.service.mock';
import { ApplicationsSearchComponent } from './applications-search.component';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

describe('ApplicationsSearchComponent', () => {
  let component: ApplicationsSearchComponent;
  let fixture: ComponentFixture<ApplicationsSearchComponent>;
  let applicationService: ApplicationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationsSearchComponent ],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: ApplicationService, useClass: ApplicationServiceMock }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsSearchComponent);
    component = fixture.componentInstance;
    applicationService = TestBed.get(ApplicationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` invokes `setupForm()`', () => {
    spyOn(component, 'setupForm');
    component.ngOnInit();
    expect(component.setupForm).toHaveBeenCalled();
  });

  it('`setupForm()` sets up `form` and then invokes `setupListener()`', () => {
    spyOn(component, 'setupListener');
    component.setupForm();
    expect(component.form).toBeDefined();
    expect(component.form.get('searchQuery').value).toEqual('');
    expect(component.setupListener).toHaveBeenCalled();
  });

  it('`setupListener()` invokes `applyFilter()`', () => {
    spyOn(component, 'applyFilter');
    component.setupListener();
    component.form.get('searchQuery').setValue('application 1');
    expect(component.applyFilter).toHaveBeenCalledWith('application 1');
  });

  it('`submitForm()` invokes `applyFilter()`', () => {
    spyOn(component, 'applyFilter');
    component.form.get('searchQuery').setValue('27/11/1990');
    component.submitForm();
    expect(component.applyFilter).toHaveBeenCalledWith('27/11/1990');
  });

  it('`applyFilter()` invokes filterEvent`', () => {
    spyOn(applicationService.filterEvent, 'next');
    component.applyFilter('joe bloggs');
    expect(applicationService.filterEvent.next).toHaveBeenCalledWith('joe bloggs');
  });

  it('`parseFilter()` returns parsed filter string`', () => {
    const parsedFilter: string = component.parseFilter('27/11/1990');
    expect(parsedFilter).toEqual('1990-11-27');
  });

  it('`parseFilter()` returns parsed filter string`', () => {
    component.ngOnDestroy();
    expect(component.componentAlive).toBeFalsy();
  });
});
