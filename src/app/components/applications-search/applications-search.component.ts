import * as moment from 'moment';
import { ApplicationService } from '../../services/application/application.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-applications-search',
  templateUrl: './applications-search.component.html',
  styleUrls: ['./applications-search.component.scss'],
})
export class ApplicationsSearchComponent implements OnInit, OnDestroy {

  public componentAlive: boolean = true;
  public form: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    private applicationService: ApplicationService,
  ) {}

  ngOnInit(): void {
    this.setupForm();
  }

  public setupForm(): void {
    this.form = this.formBuilder.group({
      searchQuery: [''],
    });

    this.setupListener();
  }

  public setupListener(): void {
    this.form.get('searchQuery').valueChanges
      .pipe(takeWhile(() => this.componentAlive))
      .subscribe((filterValue: string) => {
        this.applyFilter(filterValue);
      });
  }

  public submitForm(): void {
    const filterValue: string = this.form.get('searchQuery').value.trim();
    this.applyFilter(filterValue);
  }

  public applyFilter(filterValue: string) {
    this.applicationService.filterEvent.next(this.parseFilter(filterValue));
  }

  /**
   * Check if filterValue is a valid moment
   * Then format it so that data table can be
   * filtered on entered dates in dd/mm/yyyy format
   * @param filterValue
   */
  public parseFilter(filterValue: string): string {
    const newMoment: moment.Moment = moment(filterValue, 'DD/MM/YYYY', true);
    const filter = newMoment.isValid() ? newMoment.format('YYYY-MM-DD') : filterValue;

    return filter;
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
