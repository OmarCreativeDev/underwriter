import { AccountService } from '../../services/account/account.service';
import { AccountServiceMock } from '../../services/account/account.service.mock';
import { AccountsListComponent } from './accounts-list.component';
import { ApplicationService } from '../../services/application/application.service';
import { ApplicationServiceMock } from '../../services/application/application.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormattedSortCodePipe } from '../../shared/pipes/formatted-sort-code/formatted-sort-code.pipe';
import { RouterTestingModule } from '@angular/router/testing';

describe('AccountsListComponent', () => {
  let component: AccountsListComponent;
  let fixture: ComponentFixture<AccountsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [
        AccountsListComponent,
        FormattedSortCodePipe,
      ],
      providers: [
        { provide: AccountService, useClass: AccountServiceMock },
        { provide: ApplicationService, useClass: ApplicationServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` invokes `getPartyId()`', () => {
    spyOn(component, 'getPartyId');
    component.ngOnInit();
    expect(component.getPartyId).toHaveBeenCalled();
  });

  it('`getPartyId()` invokes `listAccounts()`', () => {
    spyOn(component, 'listAccounts');
    component.getPartyId();
    expect(component.listAccounts).toHaveBeenCalledWith(123123);
  });

  it('`listAccounts()` sets `loading` and `accounts`', () => {
    component.listAccounts(565656);
    expect(component.loading).toBeFalsy();
    expect(component.accounts).toBeDefined();
  });
});
