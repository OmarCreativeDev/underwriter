import { accountColumns, accountLabels, IAccount } from '../../services/account/account.interface';
import { AccountService } from '../../services/account/account.service';
import { ApplicationService } from '../../services/application/application.service';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss']
})
export class AccountsListComponent implements OnInit {

  public accounts: Array<IAccount>;
  public columns: Array<string> = accountColumns;
  public error: string;
  public labels: Array<string> = accountLabels;
  public loading: boolean = true;

  constructor(
    private accountService: AccountService,
    private applicationService: ApplicationService,
  ) {}

  ngOnInit(): void {
    this.getPartyId();
  }

  public getPartyId(): void {
    this.applicationService.partyIdStream
      .pipe(first())
      .subscribe((partyId: number) => {
        this.listAccounts(partyId);
      });
  }

  public listAccounts(partyId: number): void {
    this.accountService.list(partyId)
      .pipe(first())
      .subscribe((data: Array<IAccount>) => {
        this.loading = false;
        this.accounts = data;
      },
      (httpErrorResponse: HttpErrorResponse) => {
        this.loading = false;
        this.error = this.accountService.errorHandling(httpErrorResponse);
      });
  }

}
