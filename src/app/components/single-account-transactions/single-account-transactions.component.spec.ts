import { ApplicationService } from '../../services/application/application.service';
import { ApplicationServiceMock } from '../../services/application/application.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { OrderByPipe } from '../../shared/pipes/order-by/order-by.pipe';
import { RouterTestingModule } from '@angular/router/testing';
import { TransactionService } from '../../services/transaction/transaction.service';
import { transactionMock, TransactionServiceMock } from '../../services/transaction/transaction.service.mock';
import { SingleAccountTransactionsComponent } from './single-account-transactions.component';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteMock } from '../../services/activated-route.mock';
import { of } from 'rxjs';
import * as moment from 'moment';
import { MatTooltipModule } from '@angular/material';

describe('SingleAccountTransactionsComponent', () => {
  let component: SingleAccountTransactionsComponent;
  let fixture: ComponentFixture<SingleAccountTransactionsComponent>;
  let transactionService: TransactionService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SingleAccountTransactionsComponent,
        OrderByPipe,
      ],
      imports: [
        RouterTestingModule,
        MatTooltipModule,
      ],
      providers: [
        { provide: ActivatedRoute, useClass: ActivatedRouteMock },
        { provide: ApplicationService, useClass: ApplicationServiceMock },
        { provide: TransactionService, useClass: TransactionServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleAccountTransactionsComponent);
    component = fixture.componentInstance;
    transactionService = TestBed.get(TransactionService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('`ngOnInit()` should invoke `getPartyId()`', () => {
    spyOn(component, 'getPartyId');
    component.ngOnInit();
    expect(component.getPartyId).toHaveBeenCalled();
  });

  it('`getPartyId()` sets `partyId` and invokes `getOwnedItemId()`', () => {
    spyOn(component, 'getOwnedItemId');
    component.getPartyId();
    expect(component.partyId).toBeDefined();
    expect(component.partyId).toEqual(123123);
    expect(component.getOwnedItemId).toHaveBeenCalled();
  });

  it('`getOwnedItemId()` sets `ownedItemId` and invokes `getTransactions()`', () => {
    spyOn(component, 'getTransactions');
    component.getOwnedItemId();
    expect(component.ownedItemId).toBeDefined();
    expect(component.ownedItemId).toEqual(67676);
    expect(component.getTransactions).toHaveBeenCalled();
  });

  it('`getTransactions()` makes api call and on success sets loading, allTransactions and invokes `calculateDefaultPeriod()`', () => {
    spyOn(component, 'calculateDefaultPeriod');
    spyOn(transactionService, 'listTransactionsByAccount').and.returnValue(of({}));

    component.getTransactions();

    expect(component.loading).toBeFalsy();
    expect(component.allTransactions).toBeDefined();
    expect(transactionService.listTransactionsByAccount).toHaveBeenCalled();
    expect(component.calculateDefaultPeriod).toHaveBeenCalled();
  });

  it('`calculateDefaultPeriod()` should invoke `setMonthsDropdown()`', () => {
    spyOn(component, 'setMonthsDropdown');
    component.allTransactions = [transactionMock];
    component.calculateDefaultPeriod();
    expect(component.setMonthsDropdown).toHaveBeenCalled();
  });

  it('`setMonthsDropdown()` should set `periodList`', () => {
    component.setMonthsDropdown(moment());
    expect(component.periodList).toBeDefined();
    expect(component.periodList.length).toEqual(13);
  });

  it('`filterTransactions()` should set `selectedPeriod` and invoke `setTransactions()`', () => {
    component.filterTransactions('December 2018');
    expect(component.selectedPeriod).toBeDefined();
    expect(component.selectedPeriod).toEqual('December 2018');
  });
});
