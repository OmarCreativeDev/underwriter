import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../services/application/application.service';
import { Component, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ITransaction } from '../../services/transaction/transaction.interface';
import { roundedFiguresMsg } from '../../services/finance/finance.interface';
import { first } from 'rxjs/operators';
import { TransactionService } from '../../services/transaction/transaction.service';

@Component({
  selector: 'app-single-account-transactions',
  templateUrl: './single-account-transactions.component.html',
  styleUrls: ['./single-account-transactions.component.scss']
})
export class SingleAccountTransactionsComponent implements OnInit {

  public allTransactions: Array<ITransaction>;
  public error: string;
  public loading: boolean = true;
  public ownedItemId: number;
  public partyId: number;
  public periodList: Array<string>;
  public roundedFiguresMsg: string = roundedFiguresMsg;
  public selectedPeriod: string;
  public transactions: Array<ITransaction>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private applicationService: ApplicationService,
    private transactionService: TransactionService,
  ) {}

  ngOnInit(): void {
    this.getPartyId();
  }

  public getPartyId(): void {
    this.applicationService.partyIdStream
      .pipe(first())
      .subscribe((partyId: number) => {
        this.partyId = partyId;
        this.getOwnedItemId();
      });
  }

  public getOwnedItemId(): void {
    this.activatedRoute.params
      .pipe(first())
      .subscribe((data: Object) => {
        this.ownedItemId = data['ownedItemId'];
        this.getTransactions();
      });
  }

  public getTransactions(): void {
    this.transactionService.listTransactionsByAccount(this.ownedItemId, this.partyId)
      .pipe(first())
      .subscribe((data: Array<ITransaction>) => {
        this.loading = false;
        this.allTransactions = data;
        this.calculateDefaultPeriod();
      },
      (httpErrorResponse: HttpErrorResponse) => {
        this.loading = false;
        this.error = `${httpErrorResponse.statusText}, code: ${httpErrorResponse.status}`;
      });
  }

  /**
   * Calculate most recent month alongside year from transactions data
   * And set as default selected period in order to show transactions in ui
   * Then invoke setMonthsDropdown method
   */
  public calculateDefaultPeriod(): void {
    const moments: Array<moment.Moment> = this.allTransactions.map(item => moment(item.date));
    const recentMoment: moment.Moment = moment.max(moments);
    this.selectedPeriod = recentMoment.format('MMMM YYYY');

    this.setMonthsDropdown(recentMoment);
    this.setTransactions();
  }

  /**
   * Generate list of months
   * In order to populate dropdown
   * @param recentMoment
   */
  public setMonthsDropdown(recentMoment: moment.Moment): void {
    this.periodList = [];
    this.periodList.push(recentMoment.format('MMMM YYYY'));
    let previousPeriod: moment.Moment = recentMoment;

    // prepend 12 months to period list from most recent period
    for (let i = 0; i < 12; i++) {
      previousPeriod = moment(previousPeriod).subtract(1, 'months');
      this.periodList.push(previousPeriod.format('MMMM YYYY'));
    }
  }

  /**
   * Filter transactions based upon period
   * i.e month and year - e.g '10 2018'
   */
  private setTransactions(): void {
    this.transactions = this.allTransactions.filter((item: ITransaction) => {
      return moment(item.date).format('MMMM YYYY') === this.selectedPeriod;
    });
  }

  /**
   * This method is called from the ui
   * And sets selectedPeriod as per user selection
   * Then calls the filters transactions method
   * @param period
   */
  public filterTransactions(period: string): void {
    this.selectedPeriod = period.trim();
    this.setTransactions();
  }

}
