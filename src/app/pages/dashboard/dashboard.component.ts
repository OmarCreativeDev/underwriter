import { ActivatedRoute } from '@angular/router';
import { ApplicationService } from '../../services/application/application.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnInit, OnDestroy {

  private componentAlive: boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private applicationService: ApplicationService,
  ) {}

  ngOnInit(): void {
    this.setPartyId();
  }

  private setPartyId(): void {
    this.activatedRoute.params
      .pipe(takeWhile(() => this.componentAlive))
      .subscribe((data: Object) => {
        this.applicationService.partyIdStream.next(data['partyId']);
      });
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  ngOnDestroy(): void {
    this.componentAlive = false;
  }

}

