import { ApplicationsComponent } from './applications.component';
import { ApplicationService } from '../../services/application/application.service';
import { ApplicationServiceMock } from '../../services/application/application.service.mock';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterTestingModule } from '@angular/router/testing';
import { WindowRefService } from '../../services/window-ref/window-ref.service';
import { WindowRefServiceMock } from '../../services/window-ref/window-ref.service.mock';

describe('ApplicationsComponent', () => {
  let component: ApplicationsComponent;
  let fixture: ComponentFixture<ApplicationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplicationsComponent ],
      imports: [ RouterTestingModule ],
      providers: [
        { provide: ApplicationService, useClass: ApplicationServiceMock },
        { provide: WindowRefService, useClass: WindowRefServiceMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
