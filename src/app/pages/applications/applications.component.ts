import { ApplicationService } from '../../services/application/application.service';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { IApplication } from '../../services/application/application.interface';
import { Router } from '@angular/router';
import { takeWhile } from 'rxjs/operators';
import { WindowRefService } from '../../services/window-ref/window-ref.service';

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
})
export class ApplicationsComponent implements OnInit, OnDestroy {

  private componentAlive: boolean = true;
  public applications: Array<IApplication>;
  public error: string;
  public loading: boolean = true;

  constructor(
    private applicationService: ApplicationService,
    private router: Router,
    private windowRefService: WindowRefService,
  ) {}

  ngOnInit(): void {
    this.checkForPartyId();
  }

  private checkForPartyId(): void {
    if (this.partyIdExists() && !this.applicationService.hasVisitedApplicationDetails) {
      this.applicationService.hasVisitedApplicationDetails = true;
      this.navigateToApplicationDetails();
    } else {
      this.listApplications();
    }
  }

  private partyIdExists(): boolean {
    return this.windowRefService.nativeWindow.hasOwnProperty('partyId') ? true : false;
  }

  private listApplications(): void {
    this.applicationService.list()
      .pipe(takeWhile(() => this.componentAlive))
      .subscribe((data: Array<IApplication>) => {
        this.loading = false;
        this.applications = data;
      },
      (httpErrorResponse: HttpErrorResponse) => {
        this.loading = false;
        this.error = `${httpErrorResponse.statusText}, code: ${httpErrorResponse.status}`;
      });
  }

  private navigateToApplicationDetails(): void {
    this.router.navigate(['application/', this.windowRefService.nativeWindow.partyId]);
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
