import { AccountDetailsComponent } from './components/account-details/account-details.component';
import { AccountsComponent } from './pages/dashboard/accounts/accounts.component';
import { AccountsListComponent } from './components/accounts-list/accounts-list.component';
import { AffordabilitySummaryComponent } from './pages/dashboard/affordability-summary/affordability-summary.component';
import { ApplicationsComponent } from './pages/applications/applications.component';
import { AllAccountsTransactionsComponent } from './components/all-accounts-transactions/all-accounts-transactions.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: '/applications', pathMatch: 'full' },
  { path: 'applications', component: ApplicationsComponent },
  { path: 'application/:partyId', component: DashboardComponent,
    children: [
      { path: '', redirectTo: 'affordability-summary', pathMatch: 'full' },
      { path: 'affordability-summary', component: AffordabilitySummaryComponent },
      { path: 'accounts', component: AccountsComponent,
        children: [
          { path: '', redirectTo: 'list', pathMatch: 'full' },
          { path: 'list', component: AccountsListComponent },
          { path: 'single-account-transactions/:ownedItemId', component: AccountDetailsComponent },
          { path: 'all-accounts-transactions/:filter', component: AllAccountsTransactionsComponent }
        ]
      },
    ]
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, { useHash: true }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
