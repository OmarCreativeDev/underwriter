import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { of } from 'rxjs';

export const applicationMock = {
  createdOn: '',
  dateOfBirth: '',
  dateOfBirthFormatted: '',
  email: '',
  name: '',
  partyID: 1231232,
  postcode: '',
  referenceNumber: '',
  updatedOn: '',
};


@Injectable({
  providedIn: 'root'
})
export class ApplicationServiceMock {

  public partyIdStream: BehaviorSubject<number> = new BehaviorSubject<number>(123123);
  public filterEvent: Subject<string> = new Subject<string>();

  public list(): Observable<any> {
    return of([]);
  }

  public get(): Observable<any> {
    return of({});
  }

}
