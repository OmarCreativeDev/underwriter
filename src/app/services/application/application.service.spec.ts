import { ApplicationService } from './application.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientMock } from '../http-client.service.mock';
import { TestBed } from '@angular/core/testing';

describe('ApplicationService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: HttpClientMock },
    ],
  }));

  it('should be created', () => {
    const service: ApplicationService = TestBed.get(ApplicationService);
    expect(service).toBeTruthy();
  });
});
