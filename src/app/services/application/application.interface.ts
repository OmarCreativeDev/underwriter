export interface IApplication {
  createdOn: string;
  dateOfBirth: string;
  dateOfBirthFormatted: string;
  email?: string;
  name: string;
  partyID: number;
  postcode: string;
  referenceNumber: string;
  updatedOn: string;
}

export const applicationLabels: Array<string> = [
  'Name',
  'Date submitted',
  'Post code',
  'Date of birth',
  'Email address',
  'Reference No.',
];


export const applicationColumns: Array<string> = [
  'name',
  'createdOn',
  'postcode',
  'dateOfBirth',
  'email',
  'referenceNumber',
];
