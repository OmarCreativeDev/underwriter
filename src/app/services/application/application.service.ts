import { HttpClient } from '@angular/common/http';
import { IApplication } from './application.interface';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  public filterEvent: Subject<string> = new Subject<string>();
  public hasVisitedApplicationDetails: boolean = false;
  public partyIdStream: BehaviorSubject<number> = new BehaviorSubject<number>(null);

  constructor(
    private httpClient: HttpClient,
  ) {}

  /**
   * Make api call and fetch cases
   * Responds with array of IApplications
   */
  public list(): Observable<Array<IApplication>> {
    return this.httpClient.get<Array<IApplication>>(environment.serviceUrls.applicationsList);
  }

  /**
   * Make api call and fetch cases via partner
   * Responds with IApplication
   * @param partyId
   */
  public get(partyId: number): Observable<IApplication> {
    return this.httpClient.get<IApplication>(`${environment.serviceUrls.applicationGet}${partyId}`);
  }

}
