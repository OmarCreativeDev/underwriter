import { TestBed } from '@angular/core/testing';
import { PartnerService } from './partner.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientMock } from '../http-client.service.mock';

describe('PartnerService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: HttpClientMock },
    ],
  }));

  it('should be created', () => {
    const service: PartnerService = TestBed.get(PartnerService);
    expect(service).toBeTruthy();
  });
});
