import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerServiceMock {

  public fetchLogo(): Observable<any> {
    return of({});
  }

}
