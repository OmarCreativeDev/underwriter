import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPartnerLogo } from './partner.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PartnerService {

  constructor(
    private httpClient: HttpClient,
  ) {}

  public fetchLogo(): Observable<IPartnerLogo> {
    return this.httpClient.get<IPartnerLogo>(environment.serviceUrls.partnerLogo);
  }

}
