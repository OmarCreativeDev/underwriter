import { of } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ActivatedRouteMock {

  public get params() {
    return of({
      partyId: 123123,
      ownedItemId: 67676,
      filter: 'Entertainment',
    });
  }

}
