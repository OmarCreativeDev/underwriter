import { Observable, Subject } from 'rxjs';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IFinanceRecord, IFinanceMonth, FinanceTypes } from './finance.interface';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { sortBy } from 'lodash';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class FinanceService {

  constructor(
    private httpClient: HttpClient,
    private datePipe: DatePipe,
  ) {}

  public affordabilitySummariesStream = new Subject<Array<IFinanceRecord> | HttpErrorResponse>();
  public bankAccountSummariesStream = new Subject<Array<IFinanceRecord> | HttpErrorResponse>();

  public generateMonths(months: Array<IFinanceMonth>, fullNames?: boolean): Array<string> {
    const format: string = fullNames ? 'MMMM' : 'MMM';
    const monthLabels = [];

    months.forEach((item: IFinanceMonth) => {
      monthLabels.push(this.datePipe.transform(item.month, format));
    });

    return monthLabels;
  }

  private toSummary(summary: IFinanceRecord): IFinanceRecord {
    summary.subTransactionsCategorySummaries = [];
    summary.metric = FinanceTypes[summary.metric];

    // round off amount
    summary.data.map((item: IFinanceMonth) => item.totalAmount = Math.round(item.totalAmount));

    // sort data by desc order
    summary.data = sortBy(summary.data, (item: IFinanceMonth) => {
      return moment(item.month);
    }).reverse();

    return summary;
  }

  /**
   * Make api call and fetch summaries by affordability
   * Responds with array of IFinanceRecord
   * @param partyId
   * @param isIncome
   */
  public listAffordabilitySummaries(partyId: number, isIncome?: boolean): Observable<Array<IFinanceRecord>> {
    let url: string = `${environment.serviceUrls.affordabilitySummaries}${partyId}/summary`;

    if (isIncome) {
      url += `?isIncome=${isIncome}`;
    }

    return this.httpClient.get<Array<IFinanceRecord>>(url)
      .pipe(map((summaries: Array<IFinanceRecord>) => {
        return summaries.map((summary: IFinanceRecord) => this.toSummary(summary));
      }));
  }

  /**
   * Make api call and fetch summaries by bank account
   * Responds with array of IFinanceRecord
   * @param partyId
   * @param ownedItemId
   */
  public listBankAccountSummaries(partyId: number, ownedItemId: number): Observable<Array<IFinanceRecord>> {
    return this.httpClient.get<Array<IFinanceRecord>>(
      `${environment.serviceUrls.bankAccountSummaries}${partyId}/${ownedItemId}/summary`
    ).pipe(map((summaries: Array<IFinanceRecord>) => {
      return summaries.map((summary: IFinanceRecord) => this.toSummary(summary));
    }));
  }

  /**
   * Make api call and fetch affordability details
   * Responds with array of IFinanceRecord
   * @param partyId
   * @param categoryCode
   * @param isIncome
   */
  public getAffordabilityDetails(
    partyId: number,
    categoryCode: number,
    isIncome: boolean,
  ): Observable<Array<IFinanceRecord>> {
    return this.httpClient.get<Array<IFinanceRecord>>(
      `${environment.serviceUrls.affordabilityDetails}${partyId}/summary?categoryCode=${categoryCode}&isIncome=${isIncome}`
    ).pipe(map((summaries: Array<IFinanceRecord>) => {
      return summaries.map((summary: IFinanceRecord) => this.toSummary(summary));
    }));
  }

  /**
   * Make api call and fetch bank account details
   * Responds with array of IFinanceRecord
   * @param partyId
   * @param ownedItemId
   * @param categoryCode
   * @param isIncome
   */
  public getBankAccountDetails(
    partyId: number,
    ownedItemId: number,
    categoryCode: number,
    isIncome: boolean,
  ): Observable<Array<IFinanceRecord>> {
    return this.httpClient.get<Array<IFinanceRecord>>(
      `${environment.serviceUrls.bankAccountDetails}${partyId}/${ownedItemId}/summary?categoryCode=${categoryCode}&isIncome=${isIncome}`
    ).pipe(map((summaries: Array<IFinanceRecord>) => {
      return summaries.map((summary: IFinanceRecord) => this.toSummary(summary));
    }));
  }

}
