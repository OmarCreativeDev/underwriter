import { FinanceService } from './finance.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientMock } from '../http-client.service.mock';
import { TestBed } from '@angular/core/testing';
import { DatePipe } from '@angular/common';
import { DatePipeMock } from '../../shared/pipes/date.pipe.mock';

describe('FinanceService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: HttpClientMock },
      { provide: DatePipe, useClass: DatePipeMock },
    ],
  }));

  it('should be created', () => {
    const service: FinanceService = TestBed.get(FinanceService);
    expect(service).toBeTruthy();
  });
});
