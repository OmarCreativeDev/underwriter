export interface IFinanceRecord {
  averageAmount: number;
  data: Array<IFinanceMonth>;
  metric: string;
  isIncome: boolean;
  order: number;
  showDetails?: boolean;
  subTransactionsCategorySummaries?: Array<IFinanceRecord>;
}

export interface IFinanceMonth {
  month: string;
  totalAmount: any;
}

export enum FinanceTypes {
  COMMITTED = 'Committed',
  ESSENTIAL = 'Essential',
  IRREGULAR_INCOME = 'Irregular income',
  OTHER = 'Other',
  QUALITY_OF_LIFE = 'Quality of life',
  REGULAR_INCOME = 'Regular income',
}

export const roundedFiguresMsg: string = 'Note all figures are rounded.';
