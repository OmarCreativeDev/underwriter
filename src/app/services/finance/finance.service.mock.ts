import { HttpErrorResponse } from '@angular/common/http';
import { IFinanceRecord } from './finance.interface';
import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';

export const financeMock: IFinanceRecord = {
  averageAmount: 123,
  metric: 'Expense',
  order: 2,
  data: [{
    month: '',
    totalAmount: 2323,
  }],
  isIncome: true,
  showDetails: false,
  subTransactionsCategorySummaries: []
};

@Injectable({
  providedIn: 'root'
})
export class FinanceServiceMock {

  public bankAccountSummariesStream = new Subject<Array<IFinanceRecord> | HttpErrorResponse>();
  public affordabilitySummariesStream = new Subject<Array<IFinanceRecord> | HttpErrorResponse>();

  public listAffordabilitySummaries(): Observable<any> {
    return of(financeMock);
  }

  public listBankAccountSummaries(): Observable<any> {
    return of(financeMock);
  }

  public generateMonths(): Array<string> {
    return [ 'Jan', 'Feb', 'Mar' ];
  }

  public toSummary(): void {}

}
