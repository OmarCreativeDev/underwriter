import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * The logout button has to live within the MED wrapper thats outside the angular SPA
   * Therefore the angular SPA has to locate the hidden button and click it via accessing the DOM
   */
  public logout(): void {
    document.getElementById('signOutButton').click();
  }

}
