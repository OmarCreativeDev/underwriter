import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientMock } from '../http-client.service.mock';
import { TestBed } from '@angular/core/testing';

describe('AuthService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: HttpClientMock },
    ],
  }));

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });
});
