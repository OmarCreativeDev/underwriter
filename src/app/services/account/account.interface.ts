export interface IAccount {
  updatedDate: string;
  offeringType: string;
  currencyCode: string;
  itemValidDate: string;
  marketsName: string;
  sortCode: string;
  isJoint: boolean;
  balance: number;
  providerImageUrl: string;
  providerName: string;
  name: string;
  accountReference: string;
  itemId: string;
  consentId: number;
  ownedItemId: number;
  integrationId: string;
  consentExpirationDate: string;
  state: string;
  accountType: string;
}

export const accountLabels: Array<string> = [
  'Bank',
  'Account name',
  'Account type',
  'Account number',
  'Sort code',
  'Balance',
];

export const accountColumns: Array<string> = [
  'providerImageUrl',
  'name',
  'accountType',
  'accountReference',
  'sortCode',
  'currencyCode',
  'balance',
];
