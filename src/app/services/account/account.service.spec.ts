import { AccountService } from './account.service';
import { HttpClient } from '@angular/common/http';
import { HttpClientMock } from '../http-client.service.mock';
import { TestBed } from '@angular/core/testing';

describe('AccountService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: HttpClientMock },
    ],
  }));

  it('should be created', () => {
    const service: AccountService = TestBed.get(AccountService);
    expect(service).toBeTruthy();
  });
});
