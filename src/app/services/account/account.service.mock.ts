import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

export const accountMock = {
  updatedDate: '',
  offeringType: '',
  currencyCode: '',
  itemValidDate: '',
  marketsName: '',
  sortCode: '',
  isJoint: true,
  balance: 121212,
  providerImageUrl: '',
  providerName: '',
  name: '',
  accountReference: '',
  itemId: '',
  consentId: 231232,
  ownedItemId: 23256,
  integrationId: '',
  consentExpirationDate: '',
  state: '',
  accountType: '',
};

@Injectable({
  providedIn: 'root'
})
export class AccountServiceMock {

  public get(): Observable<any> {
    return of({});
  }

  public list(): Observable<any> {
    return of([]);
  }

  public errorHandling(): string {
    return 'error';
  }

}
