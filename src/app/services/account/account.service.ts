import { environment } from '../../../environments/environment';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IAccount } from './account.interface';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(
    private httpClient: HttpClient,
  ) {}

  /**
   * Make api call and fetch accounts via partyId
   * Responds with array of IAccount
   * @param partyId
   */
  public list(partyId: number): Observable<Array<IAccount>> {
    return this.httpClient.get<Array<IAccount>>(`${environment.serviceUrls.accountsList}${partyId}`);
  }

  /**
   * Make api call and fetch account via partyId & ownedItemId
   * Responds with IAccount
   * @param partyId
   * @param ownedItemId
   */
  public get(partyId: number, ownedItemId: number): Observable<IAccount> {
    return this.httpClient.get<IAccount>(`${environment.serviceUrls.accountGet}${partyId}/${ownedItemId}`);
  }

  /**
   * Back end api throws a 404 if a user has no accounts
   * so dont treat a 404 as an error
   * @param httpErrorResponse
   */
  public errorHandling(httpErrorResponse: HttpErrorResponse): string {
    let error: string;

    if (httpErrorResponse.status === 404) {
      error = null;
    } else {
      error = `${httpErrorResponse.statusText}, code: ${httpErrorResponse.status}`;
    }

    return error;
  }

}
