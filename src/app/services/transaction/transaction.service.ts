import { CurrencyPipe, DatePipe } from '@angular/common';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ITransaction } from './transaction.interface';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(
    private httpClient: HttpClient,
    private datePipe: DatePipe,
    private currencyPipe: CurrencyPipe,
  ) {}

  /**
   /**
   * Fetch all transactions for one account
   * Responds with array of ITransactions
   * @param ownedItemId
   * @param partyId
   */
  public listTransactionsByAccount(ownedItemId: number, partyId: number): Observable<Array<ITransaction>> {
    return this.httpClient.get<Array<ITransaction>>
            (`${environment.serviceUrls.singleAccountTransactions}${ownedItemId}`)
            .pipe(map((transactions: Array<ITransaction>) => {
              return transactions.map((transaction: ITransaction) => this.toTransaction(transaction));
            }));
  }

  /**
   * Fetch all transactions for all accounts
   * Responds with array of ITransactions
   * @param partyId
   * @param filter
   */
  public listAllTransactionsByFilter(partyId: number, filter?: string): Observable<Array<ITransaction>> {
    let url: string = `${environment.serviceUrls.allAccountsTransactions}${partyId}/filter`;

    if (filter) {
      url += `?filter=${filter}`;
    }

    return this.httpClient.get<Array<ITransaction>>(url)
            .pipe(map((transactions: Array<ITransaction>) => {
              return transactions.map((transaction: ITransaction) => this.toTransaction(transaction));
            }));
  }

  private toTransaction(transaction: ITransaction): ITransaction {
    transaction.categorisationCode = this.transactionType(transaction.categorisationCode);
    transaction.amount = this.currencyPipe.transform(transaction.amount, 'GBP', 'symbol', '1.0-0');
    transaction.amount = transaction.categorisationCode === 'Debit' ? `-${transaction.amount}` : transaction.amount;
    transaction.balance = this.currencyPipe.transform(transaction.balance, 'GBP', 'symbol', '1.0-0');
    transaction.dateFormatted = this.datePipe.transform(transaction.date, 'dd MMMM yyyy');

    return transaction;
  }

  /**
   * Check if transaction is an expense or not
   * Then return transaction type
   * @param categorisationCode
   */
  public transactionType(categorisationCode: string): string {
    const transactionType: string = categorisationCode.search('EXP') > -1 ? 'Debit' : 'Credit';
    return transactionType;
  }

}
