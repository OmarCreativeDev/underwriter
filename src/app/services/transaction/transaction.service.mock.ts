import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

export const transactionMock = {
  accountName: '',
  amount: 2132,
  balance: 23212,
  categorisation: '',
  categorisationCode: '',
  date: '',
  fcaCategory: '',
  metric: '',
  ownedItemId: 89,
  provider: '',
};

@Injectable({
  providedIn: 'root'
})
export class TransactionServiceMock {

  public listTransactionsByAccount(): Observable<any> {
    return of([]);
  }

  public transactionType(): string { return 'Credit'; }

  public listAllTransactionsByFilter(): Observable<any> {
    return of([]);
  }

}
