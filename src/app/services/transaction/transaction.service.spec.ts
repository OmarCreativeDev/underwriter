import { CurrencyPipe, DatePipe } from '@angular/common';
import { CurrencyPipeMock } from '../../shared/pipes/currency.pipe.mock';
import { DatePipeMock } from '../../shared/pipes/date.pipe.mock';
import { HttpClient } from '@angular/common/http';
import { HttpClientMock } from '../http-client.service.mock';
import { TestBed } from '@angular/core/testing';
import { TransactionService } from './transaction.service';

describe('TransactionService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: HttpClientMock },
      { provide: DatePipe, useClass: DatePipeMock },
      { provide: CurrencyPipe, useClass: CurrencyPipeMock },
    ],
  }));

  it('should be created', () => {
    const service: TransactionService = TestBed.get(TransactionService);
    expect(service).toBeTruthy();
  });
});
