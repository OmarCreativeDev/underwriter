export interface ITransaction {
  accountName?: string;
  amount: any;
  balance: any;
  categorisation?: string;
  categorisationCode: string;
  date: string;
  dateFormatted?: string;
  fcaCategory: string;
  metric: string;
  ownedItemId: number;
  provider?: string;
  accountNumber?: string;
  sortCode?: string;
}

export const allAccountsTransactionsMap: Array<Object> = [
  {
    title: 'Date',
    dataKey: 'dateFormatted',
  },
  {
    title: 'Payer/Payee',
    dataKey: 'provider',
  },
  {
    title: 'Category detail',
    dataKey: 'categorisation',
  },
  {
    title: 'FCA category',
    dataKey: 'fcaCategory',
  },
  {
    title: 'Account',
    dataKey: 'accountName',
  },
  {
    title: 'Credit/Debit',
    dataKey: 'categorisationCode',
  },
  {
    title: 'Account number',
    dataKey: 'accountNumber',
  },
  {
    title: 'Sort code',
    dataKey: 'sortCode',
  },
  {
    title: 'Balance',
    dataKey: 'balance',
  },
  {
    title: 'Amount',
    dataKey: 'amount',
  },
];
