import { HttpClient } from '@angular/common/http';
import { HttpClientMock } from '../http-client.service.mock';
import { TestBed } from '@angular/core/testing';
import { WindowRefService } from './window-ref.service';

describe('WindowRefService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: HttpClient, useClass: HttpClientMock },
    ],
  }));

  it('should be created', () => {
    const service: WindowRefService = TestBed.get(WindowRefService);
    expect(service).toBeTruthy();
  });
});
