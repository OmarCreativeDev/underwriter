import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WindowRefServiceMock {
  get nativeWindow(): any {
    return false;
  }
}
