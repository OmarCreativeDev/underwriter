import { Component, OnDestroy, OnInit } from '@angular/core';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { takeWhile } from 'rxjs/operators';
import { AuthService } from './services/auth/auth.service';

@Component({
  selector: 'app-underwriter',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit, OnDestroy {

  private componentAlive: boolean = true;
  private timeOutDuration: number = 60 * 15; // 15 mins

  constructor(
    private authService: AuthService,
    private idle: Idle,
  ) {}

  ngOnInit(): void {
    this.setupListener();
    this.setupIdle();
  }

  private setupIdle(): void {
    this.idle.setIdle(this.timeOutDuration / 2);
    this.idle.setTimeout(this.timeOutDuration / 2);
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this.idle.watch();
  }

  private setupListener(): void {
    this.idle.onTimeout
      .pipe(takeWhile(() => this.componentAlive))
      .subscribe(() => {
        this.authService.logout();
      });
  }

  /**
   * Unsubscribe hook to ensure no memory leaks
   */
  ngOnDestroy(): void {
    this.componentAlive = false;
  }

}
