import { AppComponent } from './app.component';
import { AuthService } from './services/auth/auth.service';
import { AuthServiceMock } from './services/auth/auth.service.mock';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { Idle } from '@ng-idle/core';
import { TestBed, async } from '@angular/core/testing';

export class IdleMock {}

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      providers: [
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: Idle, useClass: IdleMock },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
});
