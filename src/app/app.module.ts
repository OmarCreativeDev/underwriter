import { AccountDetailsComponent } from './components/account-details/account-details.component';
import { AccountOverviewComponent } from './components/account-details/account-overview/account-overview.component';
import { AccountsComponent } from './pages/dashboard/accounts/accounts.component';
import { AccountsListComponent } from './components/accounts-list/accounts-list.component';
import { AffordabilityAndAccountsNavigatorComponent } from './components/affordability-and-accounts-navigator/affordability-and-accounts-navigator.component';
import { AffordabilitySummaryComponent } from './pages/dashboard/affordability-summary/affordability-summary.component';
import { AppComponent } from './app.component';
import { ApplicationDetailsComponent } from './components/application-details/application-details.component';
import { ApplicationsComponent } from './pages/applications/applications.component';
import { ApplicationsListComponent } from './components/applications-list/applications-list.component';
import { ApplicationsSearchComponent } from './components/applications-search/applications-search.component';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { FinanceGraphComponent } from './components/finance-summaries/finance-graph/finance-graph.component';
import { FinanceSummariesComponent } from './components/finance-summaries/finance-summaries.component';
import { FinanceTableHeaderComponent } from './components/finance-summaries/finance-table-header/finance-table-header.component';
import { FinanceTableRecordComponent } from './components/finance-summaries/finance-table-record/finance-table-record.component';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/modules/shared/shared.module';
import { AllAccountsTransactionsComponent } from './components/all-accounts-transactions/all-accounts-transactions.component';
import { SingleAccountTransactionsComponent } from './components/single-account-transactions/single-account-transactions.component';

@NgModule({
  declarations: [
    AccountDetailsComponent,
    AccountOverviewComponent,
    AccountsComponent,
    AccountsListComponent,
    AffordabilityAndAccountsNavigatorComponent,
    AffordabilitySummaryComponent,
    AppComponent,
    ApplicationDetailsComponent,
    ApplicationsComponent,
    ApplicationsListComponent,
    ApplicationsSearchComponent,
    DashboardComponent,
    FinanceGraphComponent,
    FinanceSummariesComponent,
    FinanceTableHeaderComponent,
    FinanceTableRecordComponent,
    AllAccountsTransactionsComponent,
    SingleAccountTransactionsComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    NgIdleKeepaliveModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
