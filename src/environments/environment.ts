// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const API_GATEWAY = 'http://www.mocky.io/v2';
// 404 response = 5bc8810c320000750059ff8c

export const environment = {
  production: false,
  serviceUrls: {
    // applications
    applicationsList: `${API_GATEWAY}/5bcdb4e92f00005500c8550b?applicationsList&mocky-delay=500ms&partner=`,
    applicationGet: `${API_GATEWAY}/5bb4a317330000bf36cad72c?applicationGet&mocky-delay=500ms&partyId=`,
    // accounts
    accountsList: `${API_GATEWAY}/5bb5ebc73000005000aabf3a?accountsList&mocky-delay=500ms&partyId=`,
    accountGet: `${API_GATEWAY}/5bb627b53000008500aac0c4?accountGet&mocky-delay=500ms&partyId=`,
    // finance
    affordabilityDetails: `${API_GATEWAY}/5c4987c532000028350b58b7?affordabilityDetails&mocky-delay=500ms&partyId=`,
    affordabilitySummaries: `${API_GATEWAY}/5c4987c532000028350b58b7?affordabilitySummaries&mocky-delay=500ms&partyId=`,
    bankAccountDetails: `${API_GATEWAY}/5c4987c532000028350b58b7?bankAccountDetails&mocky-delay=500ms&partyId=`,
    bankAccountSummaries: `${API_GATEWAY}/5c4987c532000028350b58b7?bankAccountSummaries&mocky-delay=500ms&partyId=`,
    // transactions
    singleAccountTransactions: `${API_GATEWAY}/5c4849c0310000ca578a216f?singleAccountTransactions&mocky-delay=500ms&partyId=`,
    allAccountsTransactions: `${API_GATEWAY}/5c0e94232e0000192c043f76?5c0e94232e0000192c043f76&mocky-delay=500ms&partyId=`,
    // partner
    partnerLogo: `${API_GATEWAY}/5bd6cf013500007607fd7d54?partnerLogo&mocky-delay=500ms`,
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
