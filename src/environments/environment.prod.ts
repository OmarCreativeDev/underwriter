export const environment = {
  production: true,
  serviceUrls: {
    // applications
    applicationsList: `/api/Case/GetCasesByPartnerName`,
    applicationGet: `/api/Case/GetCaseByPartyId/`,
    // accounts
    accountsList: `/api/Accounts/`,
    accountGet: `/api/Accounts/`,
    // finance
    affordabilitySummaries: `/api/Transactions/`,
    bankAccountSummaries: `/api/Transactions/`,
    affordabilityDetails: `/api/Transactions/`,
    bankAccountDetails: `/api/Transactions/`,
    // transactions
    singleAccountTransactions: `/api/Transactions/`,
    allAccountsTransactions: `/api/Transactions/`,
    // partner
    partnerLogo: `/api/Partner`,
  },
};
