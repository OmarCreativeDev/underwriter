const API_GATEWAY = 'https://test-underwriter.runpath.com';

export const environment = {
  production: false,
  serviceUrls: {
    // applications
    applicationsList: `${API_GATEWAY}/api/Case/GetCasesByPartnerName/`,
    applicationGet: `${API_GATEWAY}/api/Case/GetCaseByPartyId/`,
    // accounts
    accountsList: `${API_GATEWAY}/api/Accounts/`,
    accountGet: `${API_GATEWAY}/api/Accounts/`,
    // finance
    affordabilitySummaries: `${API_GATEWAY}/api/Transactions/`,
    bankAccountSummaries: `${API_GATEWAY}/api/Transactions/`,
    affordabilityDetails: `${API_GATEWAY}/api/Transactions/`,
    bankAccountDetails: `${API_GATEWAY}/api/Transactions/`,
    // transactions
    singleAccountTransactions: `${API_GATEWAY}/api/Transactions/`,
    allAccountsTransactions: `${API_GATEWAY}/api/Transactions/`,
    // partner
    partnerLogo: `${API_GATEWAY}/api/Partner`,
  },
};
